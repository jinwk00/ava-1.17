package pellucid.ava.entities.throwables;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import pellucid.ava.entities.BouncingEntity;
import pellucid.ava.misc.AVASounds;

public class SmokeGrenadeEntity extends BouncingEntity
{
    public int fadeAnimation;
    public String colour;
    public int[] rgb;

    public SmokeGrenadeEntity(EntityType<? extends SmokeGrenadeEntity> type, World worldIn)
    {
        super(type, worldIn);
        this.rgb = new int[] {1, 1, 1};
    }

    public SmokeGrenadeEntity(EntityType<? extends SmokeGrenadeEntity> type, LivingEntity shooter, World worldIn, double velocity, int range, int[] rgb, String colour)
    {
        super(type, shooter, worldIn, velocity, range, AVASounds.GRENADE_HIT);
        this.ignoreFrustumCheck = true;
        this.rgb = rgb;
        this.colour = colour;
    }

    @Override
    public void tick()
    {
        super.tick();
        if (this.rangeTravelled < this.range / 4.0F)
            this.fadeAnimation++;
        else if (this.rangeTravelled > this.range / 4.0F * 3.0F)
            this.fadeAnimation--;
        if (this.rangeTravelled < this.range / 2.0F && this.rangeTravelled % 20 == 0)
            playSound(AVASounds.SMOKE_GRENADE_ACTIVE, this.rangeTravelled <= this.range / 3.0F ? 1.0F : 1.0F - 0.01F * (this.rangeTravelled - this.range / 3.0F), 1.0F);
        if (this.rangeTravelled >= this.range)
            remove();
    }

    @Override
    protected double getGravityVelocity()
    {
        return 0.0625D;
    }

    @Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putInt("fade", this.fadeAnimation);
        compound.putString("colour", this.colour);
        compound.putIntArray("rgb", this.rgb);
    }

    @Override
    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        this.fadeAnimation = compound.getInt("fade");
        this.colour = compound.getString("colour");
        this.rgb = compound.getIntArray("rgb");
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer)
    {
        super.writeSpawnData(buffer);
        buffer.writeInt(this.fadeAnimation);
        buffer.writeInt(this.range);
        buffer.writeString(this.colour);
        buffer.writeVarIntArray(this.rgb);
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData)
    {
        super.readSpawnData(additionalData);
        this.fadeAnimation = additionalData.readInt();
        this.range = additionalData.readInt();
        this.colour = additionalData.readString();
        this.rgb = additionalData.readVarIntArray();
    }
}
