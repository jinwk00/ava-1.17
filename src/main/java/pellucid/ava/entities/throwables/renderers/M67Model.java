package pellucid.ava.entities.throwables.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;

public class M67Model extends Model
{
    private final ModelRenderer bone;

    public M67Model()
    {
        super(RenderType::getEntityTranslucent);
        textureWidth = 16;
        textureHeight = 16;

        bone = new ModelRenderer(this);
        bone.setRotationPoint(0.0F, -1.0F, 0.0F);
        bone.setTextureOffset(0, 12).addBox(-1.0F, -2.5F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone.setTextureOffset(0, 0).addBox(-2.0F, -1.5F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        bone.setTextureOffset(10, 10).addBox(-1.0F, -2.5F, 2.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);
        bone.setTextureOffset(0, 8).addBox(-1.0F, -3.5F, -1.0F, 2.0F, 1.0F, 3.0F, 0.0F, false);
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        bone.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    public void func_225603_a_(float p_225603_1_, float p_225603_2_, float p_225603_3_) {
        this.bone.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
        this.bone.rotateAngleX = p_225603_3_ * ((float)Math.PI / -180F);
    }
}
