package pellucid.ava.entities.throwables;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import pellucid.ava.misc.AVADamageSource;
import pellucid.ava.misc.AVAWeaponUtil;

public class ToxicSmokeGrenadeEntity extends SmokeGrenadeEntity
{
    private int damageCD;
    public ToxicSmokeGrenadeEntity(EntityType<? extends ToxicSmokeGrenadeEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public ToxicSmokeGrenadeEntity(EntityType<? extends ToxicSmokeGrenadeEntity> type, LivingEntity shooter, World worldIn, double velocity, int range, int[] rgb, String colour)
    {
        super(type, shooter, worldIn, velocity, range, rgb, colour);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (rangeTravelled > 80 && rangeTravelled < 520)
        {
            damageCD--;
            if (damageCD <= 0)
            {
                for (Entity entity : world.getEntitiesInAABBexcluding(this, getBoundingBox().grow(3), (entity) -> entity instanceof LivingEntity))
                    AVAWeaponUtil.attackEntityDependAllyDamage(entity, AVADamageSource.causeToxicGasDamage(getShooter(), this), 5.0F);
                damageCD = 20;
            }
        }
    }

    @Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putInt("lastdamage", damageCD);
    }

    @Override
    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        damageCD = compound.getInt("lastdamage");
    }
}
