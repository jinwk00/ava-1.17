package pellucid.ava.entities.throwables;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.entities.BouncingEntity;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;

import java.util.function.BiConsumer;

public class HandGrenadeEntity extends BouncingEntity
{
    protected boolean impact;
    protected int flash;
    protected int damage;
    protected Item weapon = null;
    protected SoundEvent explosionSound;

    public HandGrenadeEntity(EntityType<? extends HandGrenadeEntity> type, World worldIn)
    {
        super(type, worldIn);
    }

    public HandGrenadeEntity(EntityType<? extends HandGrenadeEntity> type, LivingEntity shooter, World worldIn, double velocity, Item weapon, boolean impact, int damage, int flash, int range, float radius, SoundEvent explosionSound)
    {
        this(type, shooter, worldIn, velocity, weapon, impact, damage, flash, range, radius, AVASounds.GRENADE_HIT, explosionSound);
    }

    public HandGrenadeEntity(EntityType<? extends HandGrenadeEntity> type, LivingEntity shooter, World worldIn, double velocity, Item weapon, boolean impact, int damage, int flash, int range, float radius, SoundEvent collideSound, SoundEvent explosionSound)
    {
        super(type, shooter, worldIn, velocity, range, collideSound);
        this.impact = impact;
        this.flash = flash;
        this.damage = damage;
        this.weapon = weapon;
        this.explosionSound = explosionSound;
    }

    @Override
    public void tick()
    {
        super.tick();
        if (this.rangeTravelled >= this.range)
            explode();
    }

    @Override
    protected void onImpact(RayTraceResult result)
    {
        if (this.impact)
            this.explode();
        super.onImpact(result);
    }

    @Override
    protected void move()
    {
        move(!landed);
    }

    protected void explode()
    {
        explode(
                (entity, distance) -> {},
                (serverWorld, x, z) -> {
                    if (damage > 0)
                        serverWorld.spawnParticle(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.25F);
                    if (flash > 0)
                        serverWorld.spawnParticle(ParticleTypes.END_ROD, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.25F);
                }
        );
        if (flash > 0)
            flash((serverWorld, x, z) -> serverWorld.spawnParticle(ParticleTypes.END_ROD, x - 0.5F + this.rand.nextFloat(), this.getPosY() + 0.1F, z - 0.5F + this.rand.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.25F));
    }

    protected void explode(BiConsumer<Entity, Double> extraAction, TriConsumer<ServerWorld, Double, Double> effect)
    {
        AVAWeaponUtil.createExplosion(this, getShooter(), weapon,
                extraAction,
                effect,
                explosionSound);
        if (isAlive())
            remove();
    }

    protected void flash(TriConsumer<ServerWorld, Double, Double> effect)
    {
        if (flash > 0)
            AVAWeaponUtil.createFlash(this, effect, explosionSound);
        if (isAlive())
            remove();
    }

    public Item getWeapon()
    {
        return weapon;
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer)
    {
        super.writeSpawnData(buffer);
        buffer.writeString(explosionSound.getRegistryName().toString());
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData)
    {
        super.readSpawnData(additionalData);
        explosionSound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(additionalData.readString(32767)));
    }

    @Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putBoolean("impact", this.impact);
        compound.putInt("flash", this.flash);
        compound.putInt("damage", this.damage);
        compound.putString("explosionsound", explosionSound.getRegistryName().toString());
        if (this.weapon != null)
            compound.putString("weapon", this.weapon.getRegistryName().toString());
    }

    @Override
    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        impact = compound.getBoolean("impact");
        flash = compound.getInt("flash");
        damage = compound.getInt("damage");
        explosionSound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(compound.getString("explosionsound")));
        if (compound.contains("weapon"))
        {
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(compound.getString("weapon")));
            weapon = item == Items.AIR ? null : item;
        }
    }
}
