package pellucid.ava.entities.shootables.renderers;// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;

public class M202RocketModel extends Model
{
	private final ModelRenderer bone;
	private final ModelRenderer hexadecagon_r1;
	private final ModelRenderer hexadecagon_r2;
	private final ModelRenderer hexadecagon_r3;
	private final ModelRenderer hexadecagon_r4;
	private final ModelRenderer hexadecagon_r12_r1;
	private final ModelRenderer hexadecagon_r11_r1;
	private final ModelRenderer hexadecagon_r10_r1;
	private final ModelRenderer hexadecagon_r9_r1;

	public M202RocketModel()
	{
		super(RenderType::getEntityTranslucent);
		textureWidth = 512;
		textureHeight = 512;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0286F, -0.7472F, 11.2258F);
		bone.setTextureOffset(6, 11).addBox(-1.4373F, -6.0528F, -46.0591F, 3.0F, 16.0F, 42.0F, 0.0F, false);
		bone.setTextureOffset(6, 11).addBox(-8.0286F, 0.5385F, -46.0591F, 16.0F, 3.0F, 42.0F, 0.0F, false);
		bone.setTextureOffset(0, 0).addBox(-1.6362F, -5.0528F, -55.0591F, 3.0F, 14.0F, 60.0F, 0.0F, false);
		bone.setTextureOffset(0, 0).addBox(-7.0286F, 0.3396F, -55.0591F, 14.0F, 3.0F, 60.0F, 0.0F, false);
		bone.setTextureOffset(6, 11).addBox(-0.8351F, -4.0528F, 4.9409F, 2.0F, 12.0F, 26.0F, 0.0F, false);
		bone.setTextureOffset(6, 11).addBox(-6.0286F, 1.1407F, 4.9409F, 12.0F, 2.0F, 26.0F, 0.0F, false);
		bone.setTextureOffset(0, 0).addBox(-13.5286F, 0.9526F, 31.7742F, 27.0F, 1.0F, 10.0F, 0.0F, false);
		bone.setTextureOffset(0, 0).addBox(-1.0231F, -11.5528F, 31.7742F, 1.0F, 27.0F, 10.0F, 0.0F, false);

		hexadecagon_r1 = new ModelRenderer(this);
		hexadecagon_r1.setRotationPoint(-0.0286F, 1.9472F, 36.7742F);
		bone.addChild(hexadecagon_r1);
		setRotationAngle(hexadecagon_r1, 0.0F, 0.0F, -0.7854F);
		hexadecagon_r1.setTextureOffset(0, 0).addBox(-0.9946F, -13.5F, -5.0F, 1.0F, 27.0F, 10.0F, 0.0F, false);

		hexadecagon_r2 = new ModelRenderer(this);
		hexadecagon_r2.setRotationPoint(-0.0286F, 1.9472F, 36.7742F);
		bone.addChild(hexadecagon_r2);
		setRotationAngle(hexadecagon_r2, 0.0F, 0.0F, -0.3927F);
		hexadecagon_r2.setTextureOffset(0, 0).addBox(-0.9946F, -5.0F, -5.0F, 1.0F, 10.0F, 10.0F, 0.0F, false);
		hexadecagon_r2.setTextureOffset(0, 0).addBox(-5.0F, -0.9946F, -5.0F, 10.0F, 1.0F, 10.0F, 0.0F, false);

		hexadecagon_r3 = new ModelRenderer(this);
		hexadecagon_r3.setRotationPoint(-0.0286F, 1.9472F, 36.7742F);
		bone.addChild(hexadecagon_r3);
		setRotationAngle(hexadecagon_r3, 0.0F, 0.0F, 0.3927F);
		hexadecagon_r3.setTextureOffset(0, 0).addBox(-0.9946F, -5.0F, -5.0F, 1.0F, 10.0F, 10.0F, 0.0F, false);

		hexadecagon_r4 = new ModelRenderer(this);
		hexadecagon_r4.setRotationPoint(-0.0286F, 1.9472F, 36.7742F);
		bone.addChild(hexadecagon_r4);
		setRotationAngle(hexadecagon_r4, 0.0F, 0.0F, 0.7854F);
		hexadecagon_r4.setTextureOffset(0, 0).addBox(-0.9946F, -13.5F, -5.0F, 1.0F, 27.0F, 10.0F, 0.0F, false);

		hexadecagon_r12_r1 = new ModelRenderer(this);
		hexadecagon_r12_r1.setRotationPoint(16.0935F, 18.0692F, -11.2258F);
		bone.addChild(hexadecagon_r12_r1);
		setRotationAngle(hexadecagon_r12_r1, 0.0F, 0.0F, -0.7854F);
		hexadecagon_r12_r1.setTextureOffset(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, 0.0F, false);
		hexadecagon_r12_r1.setTextureOffset(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, 0.0F, false);
		hexadecagon_r12_r1.setTextureOffset(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, 0.0F, false);

		hexadecagon_r11_r1 = new ModelRenderer(this);
		hexadecagon_r11_r1.setRotationPoint(-16.1506F, 18.0692F, -11.2258F);
		bone.addChild(hexadecagon_r11_r1);
		setRotationAngle(hexadecagon_r11_r1, 0.0F, 0.0F, 0.7854F);
		hexadecagon_r11_r1.setTextureOffset(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, 0.0F, false);
		hexadecagon_r11_r1.setTextureOffset(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, 0.0F, false);
		hexadecagon_r11_r1.setTextureOffset(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, 0.0F, false);

		hexadecagon_r10_r1 = new ModelRenderer(this);
		hexadecagon_r10_r1.setRotationPoint(8.6966F, 23.0116F, -11.2258F);
		bone.addChild(hexadecagon_r10_r1);
		setRotationAngle(hexadecagon_r10_r1, 0.0F, 0.0F, -0.3927F);
		hexadecagon_r10_r1.setTextureOffset(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, 0.0F, false);
		hexadecagon_r10_r1.setTextureOffset(6, 11).addBox(-6.0F, -23.6065F, 16.1666F, 12.0F, 2.0F, 26.0F, 0.0F, false);
		hexadecagon_r10_r1.setTextureOffset(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, 0.0F, false);
		hexadecagon_r10_r1.setTextureOffset(0, 0).addBox(-7.0F, -24.4076F, -43.8334F, 14.0F, 3.0F, 60.0F, 0.0F, false);
		hexadecagon_r10_r1.setTextureOffset(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, 0.0F, false);
		hexadecagon_r10_r1.setTextureOffset(6, 11).addBox(-8.0F, -24.2087F, -34.8334F, 16.0F, 3.0F, 42.0F, 0.0F, false);

		hexadecagon_r9_r1 = new ModelRenderer(this);
		hexadecagon_r9_r1.setRotationPoint(-8.7538F, 23.0116F, -11.2258F);
		bone.addChild(hexadecagon_r9_r1);
		setRotationAngle(hexadecagon_r9_r1, 0.0F, 0.0F, 0.3927F);
		hexadecagon_r9_r1.setTextureOffset(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, 0.0F, false);
		hexadecagon_r9_r1.setTextureOffset(6, 11).addBox(-6.0F, -23.6065F, 16.1666F, 12.0F, 2.0F, 26.0F, 0.0F, false);
		hexadecagon_r9_r1.setTextureOffset(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, 0.0F, false);
		hexadecagon_r9_r1.setTextureOffset(0, 0).addBox(-7.0F, -24.4076F, -43.8334F, 14.0F, 3.0F, 60.0F, 0.0F, false);
		hexadecagon_r9_r1.setTextureOffset(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, 0.0F, false);
		hexadecagon_r9_r1.setTextureOffset(6, 11).addBox(-8.0F, -24.2087F, -34.8334F, 16.0F, 3.0F, 42.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
	{
		bone.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void func_225603_a_(float p_225603_1_, float p_225603_2_, float p_225603_3_)
	{
		bone.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		bone.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r1.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r1.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r2.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r2.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r3.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r3.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r4.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r4.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r12_r1.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r12_r1.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r11_r1.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r11_r1.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r10_r1.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r10_r1.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
		hexadecagon_r9_r1.rotateAngleY = p_225603_2_ * ((float)Math.PI / 180F);
		hexadecagon_r9_r1.rotateAngleX = p_225603_3_ * ((float)Math.PI / 180F);
	}
}