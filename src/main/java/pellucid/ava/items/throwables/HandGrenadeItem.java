package pellucid.ava.items.throwables;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import pellucid.ava.entities.ProjectileEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAWeaponUtil;

public class HandGrenadeItem extends ThrowableItem
{
    private final boolean impact;
    private final int flashDuration;
    private final int damage;

    public HandGrenadeItem(Properties properties, boolean impact, int flashDuration, int damage, float power, int range, float radius, EntityType<HandGrenadeEntity> type, SoundEvent sound, Recipe recipe, AVAWeaponUtil.Classification classification)
    {
        super(properties, power, range, radius, type, sound, recipe, classification);
        this.impact = impact;
        this.flashDuration = flashDuration;
        this.damage = damage / 5;
    }

    @Override
    protected ProjectileEntity getEntity(World world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss)
    {
        return new HandGrenadeEntity((EntityType<? extends HandGrenadeEntity>) type, shooter, world, velocity, this, this.impact, this.damage, this.flashDuration, this.range, this.radius, this.sound);
    }

    public int getDamage(boolean modified)
    {
        return modified ? this.damage : this.damage * 5;
    }
}
