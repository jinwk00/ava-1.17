package pellucid.ava.items.throwables;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.AVA;
import pellucid.ava.entities.ProjectileEntity;
import pellucid.ava.items.miscs.IClassification;
import pellucid.ava.items.miscs.IHasRecipe;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAItemGroups;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;

import javax.annotation.Nullable;

public abstract class ThrowableItem extends Item implements IHasRecipe, IClassification
{
    protected final float power;
    protected final int range;
    protected final float radius;
    protected final SoundEvent sound;
    protected final EntityType<? extends ProjectileEntity> type;
    protected final Recipe recipe;
    private final AVAWeaponUtil.Classification classification;

    public ThrowableItem(Properties properties, float power, int range, float radius, EntityType<? extends ProjectileEntity> type, SoundEvent sound, Recipe recipe, AVAWeaponUtil.Classification classification)
    {
        super(properties.group(AVAItemGroups.MAIN));
        this.power = power;
        this.range = range;
        this.radius = radius;
        this.type = type;
        this.sound = sound;
        this.recipe = recipe;
        this.classification = classification;
        classification.addToList(this);
        addPropertyOverride(new ResourceLocation(AVA.MODID, "hold"), (stack, world, entity) -> { boolean phase = (((ThrowableItem)stack.getItem()).initTags(stack).getBoolean("hold")); return phase ? 1F : 0.0F; });
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        CompoundNBT compound = initTags(stack);
        if (!isSelected)
            compound.putBoolean("hold", false);
    }

    public CompoundNBT initTags(ItemStack stack)
    {
        CompoundNBT compound;
        if (stack.hasTag())
            compound = stack.getTag();
        else
        {
            compound = new CompoundNBT();
            stack.setTag(compound);
        }
        return compound;
    }

    public void toss(ServerWorld world, LivingEntity shooter, ItemStack stack, boolean toss)
    {
        toss(world, shooter, null, stack, toss);
    }

    public void toss(ServerWorld world, LivingEntity shooter, @Nullable LivingEntity target, ItemStack stack, boolean toss)
    {
        if (shooter instanceof PlayerEntity)
            if (!classification.validate((PlayerEntity) shooter))
                return;
        ProjectileEntity grenade = getEntity(world, shooter, stack, ((toss) ? 0.5F : 1.5F) * this.power, toss);
        if (target != null)
            grenade.fromMob(target);
        world.addEntity(grenade);
        initTags(stack).putBoolean("hold", false);
        if (shooter instanceof PlayerEntity && !((PlayerEntity) shooter).abilities.isCreativeMode)
            stack.shrink(1);
    }

    protected abstract ProjectileEntity getEntity(World world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss);

    public void hold(ServerWorld world, PlayerEntity player, ItemStack stack)
    {
        initTags(stack).putBoolean("hold", true);
        world.playSound(null, player.getPosX(), player.getPosY(), player.getPosZ(), AVASounds.DEFAULT_PULL_GRENADE, SoundCategory.PLAYERS, 1.0F, 1.0F);
    }
    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    public int getRange()
    {
        return this.range;
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return classification;
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player)
    {
        return false;
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity)
    {
        return true;
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged)
    {
        return oldStack.getItem() != newStack.getItem();
    }
}
