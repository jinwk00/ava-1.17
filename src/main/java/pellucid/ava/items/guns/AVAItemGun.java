package pellucid.ava.items.guns;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.entities.livings.GunGuardEntity;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.items.miscs.*;
import pellucid.ava.misc.AVAItemGroups;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;
import pellucid.ava.misc.renderers.ICustomBakedModelGetter;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.UUID;

public class AVAItemGun extends Item implements IHasRecipe, IClassification, ICustomModel
{
    protected final int range;
    protected final float damage;
    protected final int ticksPerShot;
    protected final int maxAmmo;
    protected final Item magazineType;
    protected final float stability;
    protected final float accuracy;
    protected final int reloadTime;
    protected SoundEvent shootSound;
    protected SoundEvent reloadSound;
    protected SoundEvent scopeSound;
    protected ScopeTypes scopeType;
    protected final boolean auto;
    protected boolean hasCrosshair;
    protected final boolean requireAim;
    protected final boolean reloadInteractable;
    protected float mobility;
    protected float initialAccuracy;
    protected final boolean silenced;
    protected final int fireAnimation;
    protected final Recipe recipe;
    protected final int heldAnimation;
    protected final int runAnimation;
    protected final Properties properties;
    protected final AVAItemGun master;
    public ArrayList<AVAItemGun> subGuns = new ArrayList<>();
    @OnlyIn(Dist.CLIENT)
    private ICustomBakedModelGetter bakedModelgetter;
    public final float recoilRefund;
    private final Multimap<String, AttributeModifier> attributeModifiers;
    private final AVAWeaponUtil.Classification classification;

    protected static final UUID MOVEMENT_SPEED_MODIFIER = UUID.fromString("2A52D429-3349-462E-9191-78783205BFBE");

    public AVAItemGun(Properties gunProperties, Recipe recipe)
    {
        super(new Item.Properties().maxStackSize(1).group(AVAItemGroups.MAIN).maxDamage(gunProperties.maxAmmo));
        this.properties = gunProperties;
        this.magazineType = gunProperties.magazineType;
        this.maxAmmo = gunProperties.maxAmmo;
        this.range = Math.round(gunProperties.range * 1.5F);
        this.damage = gunProperties.damage / 5;
//        this.ticksPerShot = Math.round(gunProperties.speed * 20 / this.maxAmmo);
        this.ticksPerShot = Math.round(20 / gunProperties.speed);
        this.stability = 25.0F / (gunProperties.stability / 1.95F);
        this.accuracy = (100 - gunProperties.accuracy / 1.15F) / 175.0F;
        this.reloadTime = Math.round(gunProperties.reloadTime * 20.0F);
        this.shootSound = gunProperties.shootSound;
        this.reloadSound = gunProperties.reloadSound;
        this.scopeSound = gunProperties.scopeSound;
        this.scopeType = gunProperties.scopeType;
        this.auto = gunProperties.auto;
        this.hasCrosshair = gunProperties.hasCrosshair;
        this.requireAim = gunProperties.requireAim;
        this.reloadInteractable = gunProperties.reloadInteractable;
        this.mobility = gunProperties.mobility;
        this.heldAnimation = gunProperties.heldAnimation;
        this.runAnimation = gunProperties.runAnimation;
        this.initialAccuracy = gunProperties.initialAccuracy == -1 ? this.accuracy : (100 - gunProperties.initialAccuracy / 1.15F) / 175.0F;
        this.silenced = gunProperties.silenced;
        this.fireAnimation = gunProperties.fireAnimation == -1 ? this.ticksPerShot : gunProperties.fireAnimation;
        this.recipe = recipe;
        if (gunProperties.master != null)
        {
            this.master = gunProperties.master;
            gunProperties.master.subGuns.add(this);
        }
        else
        {
            this.master = this;
            this.subGuns.add(this);
        }
        this.recoilRefund = gunProperties.recoilRefund == 0 ? stability : gunProperties.recoilRefund;
        ImmutableMultimap.Builder<String, AttributeModifier> map = ImmutableMultimap.builder();
        map.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADDITION));
        map.put(SharedMonsterAttributes.MOVEMENT_SPEED.getName(), new AttributeModifier(MOVEMENT_SPEED_MODIFIER, "Speed modifier", (-100 + this.mobility) / 600.0D, AttributeModifier.Operation.ADDITION));
        this.attributeModifiers = map.build();
        this.classification = gunProperties.classification;
        classification.addToList(this);
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (!entityIn.isAlive() || !(entityIn instanceof PlayerEntity))
            return;
        CompoundNBT compound = initTags(stack);
        PlayerEntity player = (PlayerEntity) entityIn;
        IPlayerAction capability = PlayerAction.getCap(player);
        if (!isSelected)
        {
            compound.putInt("reload", 0);
            compound.putInt("run", 0);
        }
        else
        {
            if (player.isSprinting() && player.onGround && !player.isSneaking() && compound.getInt("run") == 0 && compound.getInt("reload") == 0 && !capability.isFiring())
                compound.putInt("run", 1);
            else if ((!player.isSprinting() || !player.onGround || player.isSneaking() || compound.getInt("reload") != 0 || capability.isFiring()) && compound.getInt("run") != 0)
                compound.putInt("run", 0);
            if (compound.getInt("run") > 0)
            {
                if (compound.getInt("run") >= 12)
                    compound.putInt("run", 1);
                else
                    compound.putInt("run", compound.getInt("run") + 1);
            }
        }
        if (compound.getInt("ammo") != stack.getMaxDamage() - getDamage(stack))
            stack.setDamage(this.maxAmmo - compound.getInt("ammo"));
        if (compound.getInt("ticks") > 0)
            compound.putInt("ticks", compound.getInt("ticks") - 1);
        if (compound.getInt("fire") > 0)
        {
            compound.putInt("fire", compound.getInt("fire") + 1);
            if (compound.getInt("fire") >= this.fireAnimation)
                compound.putInt("fire", 0);
        }
        if (compound.getInt("interact") > 0)
        {
            compound.putInt("reload", 0);
            compound.putInt("interact", compound.getInt("interact") + 1);
            if (compound.getInt("interact") >= 2)
                compound.putInt("interact", 0);
        }
        if (compound.getInt("reload") > 0)
        {
            compound.putInt("reload", compound.getInt("reload") + 1);
            if (compound.getInt("reload") >= this.reloadTime)
                reload(compound, (LivingEntity) entityIn, stack);
        }
    }

    public void forceReload(ItemStack stack)
    {
        stack.getOrCreateTag().putInt("ammo", getMaxAmmo());
        stack.setDamage(0);
    }

    public void reload(CompoundNBT compound, LivingEntity shooter, ItemStack stack)
    {
        boolean isPlayer = shooter instanceof PlayerEntity;
        while ((!isPlayer || (getSlotForMagazine((PlayerEntity) shooter) != -1 || ((PlayerEntity) shooter).abilities.isCreativeMode)) && compound.getInt("ammo") < this.maxAmmo && (!this.reloadInteractable || compound.getInt("interact") == 0))
        {
            int need = stack.getDamage();
            int have = this.maxAmmo - need;
            int amount = need;
            if (isPlayer && !((PlayerEntity) shooter).abilities.isCreativeMode)
            {
                int slot = getSlotForMagazine((PlayerEntity) shooter);
                if (slot == -1)
                    break;
                ItemStack mag = ((PlayerEntity) shooter).inventory.getStackInSlot(slot);
                boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                if (isAmmoKit)
                {
                    if (!((AmmoKit) mag.getItem()).refillRequired)
                        amount = need;
                    else
                    {
                        amount = mag.getMaxDamage() - stack.getDamage();
                        if (amount > need)
                        {
                            amount = need;
                            mag.setDamage(mag.getDamage() + amount);
                        }
                    }
                }
                else
                {
                    amount = this.maxAmmo - mag.getDamage();
                    if (mag.getCount() != 1)
                    {
                        mag.shrink(1);
                        ItemStack newMag = new ItemStack(this.magazineType);
                        if (amount > need)
                        {
                            newMag.setDamage(need);
                            amount = need;
                            World world = shooter.world;
                            if (!((PlayerEntity) shooter).inventory.addItemStackToInventory(newMag))
                                world.addEntity(new ItemEntity(world, shooter.getPosX(), shooter.getPosY(), shooter.getPosZ(), newMag));
                        }
                    }
                    else
                    {
                        if (amount > need)
                        {
                            mag.setDamage(mag.getDamage() + need);
                            amount = need;
                        }
                        else mag.shrink(1);
                    }
                }
            }
            compound.putInt("ammo", amount + have);
            stack.setDamage(this.maxAmmo - (amount + have));
        }
        compound.putInt("reload", 0);
    }

    public void fire(World world, LivingEntity shooter, ItemStack stack)
    {
        fire(world, shooter, null, stack);
    }

    public void fire(World world, LivingEntity shooter, @Nullable LivingEntity target, ItemStack stack)
    {
        if (firable(shooter, stack))
        {
            boolean isPlayer = shooter instanceof PlayerEntity;
            float spread = 1.5F;
            if (isPlayer)
            {
                PlayerEntity player = (PlayerEntity) shooter;
                IPlayerAction capability = PlayerAction.getCap(player);
                float multiplier = getMultiplier(player);
                spread = spread(capability, player, multiplier, true);
            }
            CompoundNBT compound = initTags(stack);
            if (!world.isRemote())
            {
                if (this.reloadInteractable)
                    compound.putInt("reload", 0);
                if (isPlayer)
                {
                    PlayerEntity player = (PlayerEntity) shooter;
                    if (!player.abilities.isCreativeMode && random.nextInt(this.silenced ? 100 : 25) == 0)
                        for (MonsterEntity monster : world.getLoadedEntitiesWithinAABB(MonsterEntity.class, player.getBoundingBox().grow(25.0F)))
                            monster.setAttackTarget(player);
                }
                AVAWeaponUtil.playAttenuableSoundToClientMoving(getShootSound(), shooter);
                summonBullet(world, shooter, target, spread);
                compound.putInt("ticks", this.ticksPerShot);
                if (!isPlayer || !((PlayerEntity) shooter).abilities.isCreativeMode)
                {
                    compound.putInt("ammo", compound.getInt("ammo") - 1);
                    stack.setDamage(stack.getDamage() + 1);
                }
            }
            compound.putInt("fire", 1);
            if (isPlayer)
            {
                PlayerEntity player = (PlayerEntity) shooter;
                IPlayerAction capability = PlayerAction.getCap(player);
                float multiplier = getMultiplier(player);
                recoil(capability, player, multiplier);
                shake(capability, player, multiplier);
            }
            if (compound.getInt("ammo") == 0)
                if (reloadable(shooter, stack))
                    preReload(shooter, stack);
        }
    }

    public float getMultiplier(PlayerEntity player)
    {
        float multiplier = 1;
        if (!player.onGround)
            multiplier *= 1.95;
        if (player.isSneaking())
            multiplier *= 0.55;
        return multiplier;
    }

    protected void summonBullet(World world, LivingEntity shooter, float spread)
    {
        summonBullet(world, shooter, null, spread);
    }

    protected void summonBullet(World world, LivingEntity shooter, @Nullable LivingEntity target, float spread)
    {
        if (shooter instanceof GunGuardEntity)
            spread = ((GunGuardEntity) shooter).getSpread(spread);
        BulletEntity bullet = new BulletEntity(world, shooter, this, this.range, this.damage, spread);
        if (target != null)
            bullet.fromMob(target);
        world.addEntity(bullet);
    }

    public void preReload(LivingEntity shooter, ItemStack stack)
    {
        CompoundNBT compound = initTags(stack);
        if (reloadable(shooter, stack))
        {
            if (!shooter.world.isRemote())
                shooter.getEntityWorld().playMovingSound(null, shooter, getReloadSound(), SoundCategory.PLAYERS, 1.0F, 1.0F);
            compound.putInt("reload", 1);
            compound.putInt("fire", 0);
            if (shooter instanceof PlayerEntity)
            {
                IPlayerAction capability = PlayerAction.getCap((PlayerEntity) shooter);
                if (capability.isAiming())
                    capability.setAiming(false);
            }
        }
    }

    public float spread(IPlayerAction capability, PlayerEntity player, float multiplier, boolean modify)
    {
        float spread = capability.getSpread() + this.accuracy * multiplier * 0.35F;
        if (capability.getSpread() <= 0 && !requireAim)
            spread += initialAccuracy * multiplier * 0.5F;
        else if (this.requireAim)
        {
            if (!capability.isAiming() || !player.onGround)
                spread = 5.0F * multiplier;
            else
                spread = accuracy * multiplier;
        }
        else
            spread = Math.min(spread, this.stability * 2.25F);
        if (modify)
            capability.setSpread(spread);
        return spread;
    }

    public void recoil(IPlayerAction capability, PlayerEntity player, float multiplier)
    {
        float pitch = -1 * capability.getRecoil() * this.stability * 0.155F - this.stability / 2.0F;
        if (capability.getRecoil() == 0)
            pitch -= this.stability / 20.0F;
        pitch *= multiplier;
        float recoil = capability.getRecoil() + Math.abs(pitch);
        if (recoil > this.stability * 17 || recoil > 20)
        {
            recoil = Math.min(this.stability * 17, 20);
            pitch = (recoil - capability.getRecoil()) * -1;
        }
        capability.setRecoil(recoil);
        player.rotationPitch += pitch;
    }

    public void shake(IPlayerAction capability, PlayerEntity player, float multiplier)
    {
        float yaw = (random.nextFloat()) * this.stability * capability.getRecoil() * 0.5F;
        yaw *= multiplier;
        yaw *= random.nextBoolean() ? 1.0F : -1.0F;
        float shake = capability.getShake() + yaw;
        if (Math.abs(shake) > stability * 6)
        {
            if (capability.getShake() > 0)
                shake = stability * 6.0F;
            else
                shake = -stability * 6.0F;
            yaw = (shake - capability.getShake());
        }
        capability.setShake(shake);
        player.rotationYaw += yaw;
    }

    public boolean firable(LivingEntity shooter, ItemStack stack)
    {
        CompoundNBT compound = initTags(stack);
        boolean isCreative = false;
        if (shooter instanceof PlayerEntity)
            if (!classification.validate((PlayerEntity) shooter))
                return false;
        return (compound.getInt("ammo") > 0 || isCreative) && (compound.getInt("reload") == 0 || this.reloadInteractable) && compound.getInt("ticks") == 0;
    }

    public boolean reloadable(LivingEntity shooter, ItemStack stack)
    {
        CompoundNBT compound = initTags(stack);
        boolean hasMag = true;
        if (shooter instanceof PlayerEntity)
            hasMag = getSlotForMagazine((PlayerEntity) shooter) != -1 || ((PlayerEntity) shooter).abilities.isCreativeMode;
        return hasMag && compound.getInt("reload") == 0 && compound.getInt("ammo") < this.maxAmmo && (!this.reloadInteractable || compound.getInt("interact") == 0);
    }

    protected int getSlotForMagazine(PlayerEntity player)
    {
        for (int slot=0;slot<player.inventory.getSizeInventory();slot++)
            if (player.inventory.getStackInSlot(slot).getItem() == this.magazineType || player.inventory.getStackInSlot(slot).getItem() instanceof AmmoKit)
                return slot;
        return -1;
    }

    public CompoundNBT initTags(ItemStack stack)
    {
        CompoundNBT compound;
        if (stack.hasTag())
            compound = stack.getTag();
        else
        {
            compound = new CompoundNBT();
            compound.putInt("ammo", 0);
            compound.putInt("fire", 0);
            compound.putInt("interact", 0);
            compound.putInt("run", 0);
            compound.putInt("ticks", 0);
            compound.putInt("reload", 0);
            stack.setTag(compound);
        }
        return compound;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 72000;
    }

    @Override
    public UseAction getUseAction(ItemStack stack)
    {
        return UseAction.NONE;
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity)
    {
        return true;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, PlayerEntity player, Entity entity)
    {
        return true;
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged)
    {
        return oldStack.getItem() != newStack.getItem();
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EquipmentSlotType slot, ItemStack stack)
    {
        return slot == EquipmentSlotType.MAINHAND ? this.attributeModifiers : super.getAttributeModifiers(slot, stack);
    }

    public float getAccuracy(boolean modified)
    {
        return modified ? this.accuracy : this.properties.getAccuracy();
    }

    public float getStability(boolean modified)
    {
        return modified ? this.stability : this.properties.getStability();
    }

    public SoundEvent getShootSound()
    {
        return this.shootSound;
    }

    public SoundEvent getReloadSound()
    {
        return this.reloadSound;
    }

    public SoundEvent getScopeSound()
    {
        return this.scopeSound;
    }

    public ScopeTypes getScopeType()
    {
        return this.scopeType;
    }

    public boolean isAuto()
    {
        return this.auto;
    }

    public boolean hasCrosshair()
    {
        return this.hasCrosshair;
    }

    public boolean isReloadInteractable()
    {
        return this.reloadInteractable;
    }

    public int getMaxAmmo()
    {
        return this.maxAmmo;
    }

    public float getInitialAccuracy()
    {
        return this.initialAccuracy;
    }

    public float getDamage(boolean modified)
    {
        return modified ? this.damage : this.properties.getDamage();
    }

    public int getRange(boolean modified)
    {
        return modified ? this.range : this.properties.getRange();
    }

    public float getAttackSpeed(boolean modified)
    {
        return modified ? this.ticksPerShot : this.properties.getSpeed();
    }

    public int getFireAnimation()
    {
        return this.fireAnimation;
    }

    public int getReloadTime()
    {
        return this.reloadTime;
    }

    public int getHeldAnimation()
    {
        return this.heldAnimation;
    }

    public int getRunAnimation()
    {
        return this.runAnimation;
    }

    public Item getMagazineType()
    {
        return this.magazineType;
    }

    public ArrayList<AVAItemGun> getSubGuns()
    {
        return this.subGuns;
    }

    public AVAItemGun getMaster()
    {
        return this.master;
    }

    public boolean isMaster()
    {
        return this == this.master;
    }

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IBakedModel getCustomModel(IBakedModel originalBakedModel)
    {
        return this.bakedModelgetter == null ? null : this.bakedModelgetter.getBakedModel(originalBakedModel);
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return classification;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void setCustomModel(ICustomBakedModelGetter getter)
    {
        this.bakedModelgetter = getter;
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player)
    {
        return false;
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items)
    {
        ItemStack stack = new ItemStack(this);
        stack.setDamage(stack.getMaxDamage());
        if (this.isInGroup(group))
            items.add(stack);
    }

    public static final ResourceLocation MOSIN_NAGANT_TEXTURE = new ResourceLocation(AVA.MODID + ":textures/overlay/mosin_nagant.png");
    public static final ResourceLocation M202_TEXTURE = new ResourceLocation(AVA.MODID + ":textures/overlay/m202.png");

    public enum ScopeTypes
    {
        NONE(null, 0.0F),
        GM94(null, 0.1F),
        X95R(null, 0.15F),
        M4A1(null, 0.25F),
        XM8(null, 0.3F),
        MK20(null, 0.35F),
        M202(M202_TEXTURE, 0.4F),
        SR25(MOSIN_NAGANT_TEXTURE, 0.45F),
        MOSIN_NAGANT(MOSIN_NAGANT_TEXTURE, 0.5F),
        ;
        @Nullable
        private final ResourceLocation texture;
        private final float magnification;

        ScopeTypes(@Nullable ResourceLocation texture, float magnification)
        {
            this.texture = texture;
            this.magnification = magnification;
        }

        @Nullable
        public ResourceLocation getTexture()
        {
            return texture;
        }

        public float getMagnification()
        {
            return magnification;
        }
    }

    public static class Properties
    {
        private Item magazineType = Items.SNOWBALL;
        private int maxAmmo = 30;
        private int range = 70;
        private float damage = 5.0F;
        private float speed = 7.50F;
        private float stability = 50.0F;
        private float accuracy = 50.0F;
        private float reloadTime = 1;
        private SoundEvent shootSound = SoundEvents.ENTITY_CHICKEN_AMBIENT;
        private SoundEvent reloadSound = SoundEvents.BLOCK_ANVIL_PLACE;
        private SoundEvent scopeSound = AVASounds.DEFAULT_AIM;
        private ScopeTypes scopeType = ScopeTypes.NONE;
        private boolean auto = true;
        private boolean hasCrosshair = true;
        private boolean requireAim = false;
//        private final Map<ResourceLocation, IItemPropertyGetter> properties = Maps.newHashMap();
        private boolean reloadInteractable = false;
        private float mobility = 1;
        private int fireAnimation = -1;
        private int heldAnimation = 1;
        private int runAnimation = 1;
        private float initialAccuracy = -1.0F;
        private boolean silenced = false;
        private AVAItemGun master = null;
        private float recoilRefund = 0;
        private final AVAWeaponUtil.Classification classification;

        public Properties(AVAWeaponUtil.Classification classification)
        {
            this.classification = classification;
        }

        public Properties magazineType(Item magazineType)
        {
            this.magazineType = magazineType;
            return this;
        }

        public Properties maxAmmo(int amount)
        {
            this.maxAmmo = amount;
            return this;
        }

        public Properties range(int range)
        {
            this.range = range;
            return this;
        }

        public Properties damage(float damage)
        {
            this.damage = damage;
            return this;
        }

        public Properties speed(float speed)
        {
            this.speed = speed;
            return this;
        }

        public Properties stability(float stability)
        {
            this.stability = stability;
            return this;
        }

        public Properties accuracy(float accuracy)
        {
            this.accuracy = accuracy;
            return this;
        }

        public Properties reloadTime(float time)
        {
            this.reloadTime = time;
            return this;
        }

        public Properties shootSound(SoundEvent sound)
        {
            this.shootSound = sound;
            return this;
        }

        public Properties reloadSound(SoundEvent sound)
        {
            this.reloadSound = sound;
            return this;
        }

//        public Properties scopeSound(SoundEvent sound)
//        {
//            this.scopeSound = sound;
//            return this;
//        }

        public Properties scopeType(ScopeTypes type)
        {
            this.scopeType = type;
            return this;
        }

        public Properties manual()
        {
            this.auto = false;
            return this;
        }

        public Properties noCrosshair()
        {
            this.hasCrosshair = false;
            return this;
        }

        public Properties requireAim()
        {
            this.requireAim = true;
            return this;
        }

//        public Properties addProperty(String name, IItemPropertyGetter getter)
//        {
//            this.properties.put(new ResourceLocation(AVA.MODID, name), getter);
//            return this;
//        }

        public Properties reloadInteractable()
        {
            this.reloadInteractable = true;
            return this;
        }

        public Properties mobility(float mobility)
        {
            this.mobility = mobility;
            return this;
        }

        public Properties initialAccuracy(float accuracy)
        {
            this.initialAccuracy = accuracy;
            return this;
        }

        public Properties silenced()
        {
            this.silenced = true;
            return this;
        }

        public Properties fireAnimationTime(int ticks)
        {
            this.fireAnimation = ticks;
            return this;
        }

        public Properties heldAnimation(int value)
        {
            this.heldAnimation = value;
            return this;
        }

        public Properties runAnimation(int value)
        {
            this.runAnimation = value;
            return this;
        }

//        public Properties regularRunAnimation()
//        {
//            this.addProperty("run", (stack, world, entity) -> { int phase = (((AVAItemGun)stack.getItem()).initTags(stack).getInt("run"));
//                if (entity == null || phase == 0)
//                    return 0.0F;
//                if (phase <= 3)
//                    return 1F;
//                if (phase <= 6)
//                    return 2F;
//                if (phase <= 9)
//                    return 3F;
//                if (phase <= 12)
//                    return 2F;
//                return 0.0F; });
//            return this;
//        }

        public Properties skinned(AVAItemGun master)
        {
            this.master = master;
            return this;
        }

//        public Properties regularFireAnimation()
//        {
//            this.addProperty("fire", (stack, world, entity) -> {
//            int phase = (((AVAItemGun)stack.getItem()).initTags(stack).getInt("fire"));
//            return entity == null || world == null || phase == 0 ? 0.0F : (float) (world.rand.nextInt(2) + 1); });
//            return this;
//        }

        public Properties recoilRefund(float value)
        {
            this.recoilRefund = value;
            return this;
        }

        public int getMaxAmmo()
        {
            return this.maxAmmo;
        }

        public int getRange()
        {
            return this.range;
        }

        public float getDamage()
        {
            return this.damage;
        }

        public float getStability()
        {
            return this.stability;
        }

        public float getAccuracy()
        {
            return this.accuracy;
        }

        public float getSpeed()
        {
            return this.speed;
        }
    }
}
