package pellucid.ava.items.guns;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import pellucid.ava.entities.AVAEntity;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.items.miscs.Recipe;

import javax.annotation.Nullable;

public class AVASpecialWeapon extends AVAItemGun
{
    protected final ProjectileFactory projectileFactory;

    public AVASpecialWeapon(Properties gunProperties, Recipe recipe, ProjectileFactory projectileFactory)
    {
        super(gunProperties, recipe);
        this.projectileFactory = projectileFactory;
    }

    @Override
    public void reload(CompoundNBT compound, LivingEntity shooter, ItemStack stack)
    {
        boolean isPlayer = shooter instanceof PlayerEntity;
        while ((!isPlayer || (getSlotForMagazine((PlayerEntity) shooter) != -1 || ((PlayerEntity) shooter).abilities.isCreativeMode)) && compound.getInt("ammo") < this.maxAmmo && (!this.reloadInteractable || compound.getInt("interact") == 0))
        {
            int need = stack.getDamage();
            int have = this.maxAmmo - need;
            int amount = need;
            if (isPlayer && !((PlayerEntity) shooter).abilities.isCreativeMode)
            {
                int slot = getSlotForMagazine((PlayerEntity) shooter);
                if (slot == -1)
                    break;
                ItemStack mag = ((PlayerEntity) shooter).inventory.getStackInSlot(slot);
                boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                if (isAmmoKit)
                {
                    if (((AmmoKit) mag.getItem()).refillRequired)
                    {
                        amount = mag.getMaxDamage() - stack.getDamage();
                        if (amount > need)
                        {
                            amount = need;
                            mag.setDamage(mag.getDamage() + amount);
                        }
                    }
                }
                else
                    mag.shrink(Math.min(mag.getCount(), need));
            }
            compound.putInt("ammo", amount + have);
            stack.setDamage(this.maxAmmo - (amount + have));
        }
        compound.putInt("reload", 0);
    }

    @Override
    protected void summonBullet(World world, LivingEntity shooter, @Nullable LivingEntity target, float spread)
    {
        AVAEntity projectile = projectileFactory.create(world, shooter, spread, this);
        if (target != null)
            projectile.fromMob(target);
        world.addEntity(projectile);
    }

    @FunctionalInterface
    public interface ProjectileFactory
    {
        AVAEntity create(World world, LivingEntity shooter, float spread, Item weapon);
    }
}
