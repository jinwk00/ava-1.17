package pellucid.ava.items.guns;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.items.miscs.Recipe;

public class AVAManual extends AVASingle
{
    public AVAManual(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties.reloadInteractable(), recipe);
    }

    @Override
    public void reload(CompoundNBT compound, LivingEntity shooter, ItemStack stack)
    {
        boolean isPlayer = shooter instanceof PlayerEntity;
        if ((!isPlayer || (getSlotForMagazine((PlayerEntity) shooter) != -1 || ((PlayerEntity) shooter).abilities.isCreativeMode)) && compound.getInt("ammo") < this.maxAmmo && (!this.reloadInteractable || compound.getInt("interact") == 0))
        {
            if (isPlayer && !((PlayerEntity) shooter).abilities.isCreativeMode)
            {
                int slot = getSlotForMagazine((PlayerEntity) shooter);
                if (slot != -1)
                {
                    ItemStack mag = ((PlayerEntity) shooter).inventory.getStackInSlot(slot);
                    if (!(mag.getItem() instanceof AmmoKit))
                        mag.shrink(1);
                    else if (((AmmoKit) mag.getItem()).refillRequired)
                        mag.setDamage(mag.getDamage() + 1);
                }
                else
                {
                    compound.putInt("reload", 0);
                    return;
                }
            }
            compound.putInt("ammo", compound.getInt("ammo") + 1);
            stack.setDamage(stack.getDamage() - 1);
        }
        compound.putInt("reload", 0);
        preReload(shooter, stack);
    }
}
