package pellucid.ava.items.guns;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.cap.IPlayerAction;

import javax.annotation.Nullable;

public class AVAShotgun extends AVAManual
{
    private final int bullets;
    public AVAShotgun(Properties gunProperties, Recipe recipe, int bullets)
    {
        super(gunProperties, recipe);
        this.bullets = bullets;
    }

    @Override
    public float getMultiplier(PlayerEntity player)
    {
        float multiplier = 1;
        if (!player.onGround)
            multiplier *= 1.65;
        if (player.isSneaking())
            multiplier *= 0.9;
        return multiplier;
    }

    @Override
    public float spread(IPlayerAction capability, PlayerEntity player, float multiplier, boolean modify)
    {
        float spread = capability.getSpread() + this.accuracy * multiplier * 0.55F;
        if (capability.getSpread() == 0)
            if (!this.requireAim)
                spread += this.initialAccuracy * multiplier * 0.85F;
        spread = Math.min(spread, this.stability * 2.5F);
        if (modify)
            capability.setSpread(spread);
        return spread;
    }

    @Override
    protected void summonBullet(World world, LivingEntity shooter, @Nullable LivingEntity target, float spread)
    {
        for (int s = 0; s<this.bullets; s++)
            super.summonBullet(world, shooter, target, spread);
    }
}
