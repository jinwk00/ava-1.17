package pellucid.ava.items.init;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVAItemGroups;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.models.fg42.FG42DreamcatcherModel;
import pellucid.ava.misc.renderers.models.fg42.FG42Model;
import pellucid.ava.misc.renderers.models.fg42.FG42SumireModel;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncDreamcatcherModel;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncFullmoonModel;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncModel;
import pellucid.ava.misc.renderers.models.m4a1.M4A1DreamcatcherModel;
import pellucid.ava.misc.renderers.models.m4a1.M4A1SumireModel;
import pellucid.ava.misc.renderers.models.m4a1.M4A1XPlorerModel;
import pellucid.ava.misc.renderers.models.m4a1.M4a1Model;
import pellucid.ava.misc.renderers.models.mk20.MK20Model;
import pellucid.ava.misc.renderers.models.xm8.Xm8FrostModel;
import pellucid.ava.misc.renderers.models.xm8.Xm8Model;
import pellucid.ava.misc.renderers.models.xm8.Xm8SnowfallModel;

import java.util.ArrayList;

public class Rifles
{
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */
    public static Item M4A1_MAGAZINE = null;
    private static final AVAItemGun.Properties M4A1_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.RIFLE).damage(35).range(70).accuracy(80).stability(95).speed(10.5F).maxAmmo(30).mobility(95).reloadTime(2.8F)
            .shootSound(AVASounds.M4A1_SHOOT).reloadSound(AVASounds.M4A1_RELOAD).scopeType(AVAItemGun.ScopeTypes.M4A1);

    public static Item M4A1 = null;
    public static Item M4A1_SUMIRE = null;
    public static Item M4A1_DREAMCATCHER = null;
    public static Item M4A1_XPLORER = null;

    public static Item MK20_MAGAZINE = null;
    private static final AVAItemGun.Properties MK20_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.RIFLE).damage(50).range(63).accuracy(86.5F).stability(50.3F).speed(10.5F).maxAmmo(20).mobility(94.3F).reloadTime(2)
            .shootSound(AVASounds.MK20_SHOOT).reloadSound(AVASounds.MK20_RELOAD).scopeType(AVAItemGun.ScopeTypes.MK20);

    public static Item MK20 = null;

    public static Item FN_FNC_MAGAZINE = null;
    private static final AVAItemGun.Properties FN_FNC_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.RIFLE).damage(39).range(70).accuracy(76.8F).stability(85.4F).speed(10.2F).maxAmmo(30).mobility(96.2F).reloadTime(1.75F)
            .shootSound(AVASounds.FN_FNC_SHOOT).reloadSound(AVASounds.FN_FNC_RELOAD);

    public static Item FN_FNC = null;
    public static Item FN_FNC_DREAMCATCHER = null;
    public static Item FN_FNC_FULLMOON = null;

    public static Item XM8_MAGAZINE = null;
    private static final AVAItemGun.Properties XM8_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.RIFLE).damage(39.2F).range(70).accuracy(75.3F).stability(90.9F).speed(10).maxAmmo(30).mobility(93.2F).reloadTime(2)
            .shootSound(AVASounds.XM8_SHOOT).reloadSound(AVASounds.XM8_RELOAD).scopeType(AVAItemGun.ScopeTypes.XM8);

    public static Item XM8 = null;
    public static Item XM8_FROST = null;
    public static Item XM8_SNOWFALL = null;

    public static Item FG42_MAGAZINE = null;
    private static final AVAItemGun.Properties FG42_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.RIFLE).damage(47).range(68).accuracy(61.3F).stability(74.5F).speed(9.09F).maxAmmo(20).mobility(93.2F).reloadTime(2)
            .shootSound(AVASounds.FG42_SHOOT).reloadSound(AVASounds.FG42_RELOAD).initialAccuracy(96.5F);

    public static Item FG42 = null;
    public static Item FG42_SUMIRE = null;
    public static Item FG42_DREAMCATCHER = null;

    public static ArrayList<Item> ITEM_RIFLES = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                M4A1_MAGAZINE = new Ammo(new Item.Properties().maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("m4a1_magazine"),
                M4A1 = new AVAItemGun(M4A1_P.magazineType(M4A1_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 15).addItem(Items.IRON_NUGGET, 5).addItem(Items.GLASS_PANE)).setRegistryName("m4a1"),
                M4A1_SUMIRE = new AVAItemGun(M4A1_P.skinned((AVAItemGun) M4A1), new Recipe().addItem(M4A1).mergeIngredients(SharedRecipes.SUMIRE)).setRegistryName("m4a1_sumire"),
                M4A1_DREAMCATCHER = new AVAItemGun(M4A1_P.skinned((AVAItemGun) M4A1), new Recipe().addItem(M4A1).mergeIngredients(SharedRecipes.DREAMCATCHER)).setRegistryName("m4a1_dreamcatcher"),
                M4A1_XPLORER = new AVAItemGun(M4A1_P.skinned((AVAItemGun) M4A1), new Recipe().addItem(M4A1).mergeIngredients(SharedRecipes.XPLORER)).setRegistryName("m4a1_xplorer"),

                MK20_MAGAZINE = new Ammo(new Item.Properties().group(AVAItemGroups.MAIN).maxDamage(20), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("mk20_magazine"),
                MK20 = new AVAItemGun(MK20_P.magazineType(MK20_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 10).addItem(Items.GOLD_INGOT, 5).addItem(Items.GLASS_PANE)).setRegistryName("mk20"),

                FN_FNC_MAGAZINE = new Ammo(new Item.Properties().group(AVAItemGroups.MAIN).maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("fn_fnc_magazine"),
                FN_FNC = new AVAItemGun(FN_FNC_P.magazineType(FN_FNC_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 12).addItem(Items.COAL, 5)).setRegistryName("fn_fnc"),
                FN_FNC_DREAMCATCHER = new AVAItemGun(FN_FNC_P.skinned((AVAItemGun) FN_FNC), new Recipe().addItem(FN_FNC).mergeIngredients(SharedRecipes.DREAMCATCHER)).setRegistryName("fn_fnc_dreamcatcher"),
                FN_FNC_FULLMOON = new AVAItemGun(FN_FNC_P.skinned((AVAItemGun) FN_FNC), new Recipe().addItem(FN_FNC).mergeIngredients(SharedRecipes.FULLMOON)).setRegistryName("fn_fnc_fullmoon"),

                XM8_MAGAZINE = new Ammo(new Item.Properties().group(AVAItemGroups.MAIN).maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("xm8_magazine"),
                XM8 = new AVAItemGun(XM8_P.magazineType(XM8_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 15).addItem(Items.BLACK_DYE, 5).addItem(Items.GLASS_PANE)).setRegistryName("xm8"),
                XM8_FROST = new AVAItemGun(XM8_P.skinned((AVAItemGun) XM8), new Recipe().addItem(XM8).mergeIngredients(SharedRecipes.FROST)).setRegistryName("xm8_frost"),
                XM8_SNOWFALL = new AVAItemGun(XM8_P.skinned((AVAItemGun) XM8), new Recipe().addItem(XM8).mergeIngredients(SharedRecipes.SNOWFALL)).setRegistryName("xm8_snowfall"),

                FG42_MAGAZINE = new Ammo(new Item.Properties().group(AVAItemGroups.MAIN).maxDamage(20), new Recipe().addItem(Items.IRON_INGOT, 1).addItem(Items.GUNPOWDER), true).setRegistryName("fg42_magazine"),
                FG42 = new AVAItemGun(FG42_P.magazineType(FG42_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 15).addItem(Items.OAK_PLANKS, 10)).setRegistryName("fg42"),
                FG42_SUMIRE = new AVAItemGun(FG42_P.skinned((AVAItemGun) FG42), new Recipe().addItem(FG42).mergeIngredients(SharedRecipes.SUMIRE)).setRegistryName("fg42_sumire"),
                FG42_DREAMCATCHER = new AVAItemGun(FG42_P.skinned((AVAItemGun) FG42), new Recipe().addItem(FG42).mergeIngredients(SharedRecipes.DREAMCATCHER)).setRegistryName("fg42_dreamcatcher")
        );
        DistExecutor.runWhenOn(Dist.CLIENT, () -> Rifles::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((AVAItemGun) M4A1).setCustomModel((origin) -> new Model(origin, M4a1Model::new));
        ((AVAItemGun) M4A1_SUMIRE).setCustomModel((origin) -> new Model(origin, M4A1SumireModel::new));
        ((AVAItemGun) M4A1_DREAMCATCHER).setCustomModel((origin) -> new Model(origin, M4A1DreamcatcherModel::new));
        ((AVAItemGun) M4A1_XPLORER).setCustomModel((origin) -> new Model(origin, M4A1XPlorerModel::new));
        ((AVAItemGun) MK20).setCustomModel((origin) -> new Model(origin, MK20Model::new));
        ((AVAItemGun) FN_FNC).setCustomModel((origin) -> new Model(origin, FnfncModel::new));
        ((AVAItemGun) FN_FNC_DREAMCATCHER).setCustomModel((origin) -> new Model(origin, FnfncDreamcatcherModel::new));
        ((AVAItemGun) FN_FNC_FULLMOON).setCustomModel((origin) -> new Model(origin, FnfncFullmoonModel::new));
        ((AVAItemGun) XM8).setCustomModel((origin) -> new Model(origin, Xm8Model::new));
        ((AVAItemGun) XM8_FROST).setCustomModel((origin) -> new Model(origin, Xm8FrostModel::new));
        ((AVAItemGun) XM8_SNOWFALL).setCustomModel((origin) -> new Model(origin, Xm8SnowfallModel::new));
        ((AVAItemGun) FG42).setCustomModel((origin) -> new Model(origin, FG42Model::new));
        ((AVAItemGun) FG42_SUMIRE).setCustomModel((origin) -> new Model(origin, FG42SumireModel::new));
        ((AVAItemGun) FG42_DREAMCATCHER).setCustomModel((origin) -> new Model(origin, FG42DreamcatcherModel::new));
    }
}
