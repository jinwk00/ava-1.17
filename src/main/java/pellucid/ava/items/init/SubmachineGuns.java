package pellucid.ava.items.init;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVAShotgun;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.models.mk18.Mk18AirWarfareModel;
import pellucid.ava.misc.renderers.models.mk18.Mk18KuyomonModel;
import pellucid.ava.misc.renderers.models.mk18.Mk18Model;
import pellucid.ava.misc.renderers.models.mp5k.Mp5kFrostModel;
import pellucid.ava.misc.renderers.models.mp5k.Mp5kModel;
import pellucid.ava.misc.renderers.models.mp5sd5.Mp5sd5Model;
import pellucid.ava.misc.renderers.models.remington870.Reminton870DreamcatcherModel;
import pellucid.ava.misc.renderers.models.remington870.Reminton870Model;
import pellucid.ava.misc.renderers.models.x95r.X95RModel;

import java.util.ArrayList;

public class SubmachineGuns
{

    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */
    public static Item X95R_MAGAZINE = null;
    private static final AVAItemGun.Properties X95R_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SUB_MACHINEGUN).damage(39).range(45).accuracy(74.9F).stability(82.9F).speed(11.76F).maxAmmo(30).mobility(93 + 5.5F).reloadTime(1.75F)
            .shootSound(AVASounds.X95R_SHOOT).reloadSound(AVASounds.X95R_RELOAD).scopeType(AVAItemGun.ScopeTypes.X95R);

    public static Item X95R = null;

    public static Item MP5SD5_MAGAZINE = null;
    private static final AVAItemGun.Properties MP5SD5_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SUB_MACHINEGUN).damage(34.5F).range(38).accuracy(82.5F).stability(89.8F).speed(12.35F).maxAmmo(30).mobility(90.1F + 5.5F).reloadTime(1.75F)
            .shootSound(AVASounds.MP5SD5_SHOOT).reloadSound(AVASounds.MP5SD5_RELOAD).silenced();

    public static Item MP5SD5 = null;

    public static Item MK18_MAGAZINE = null;
    private static final AVAItemGun.Properties MK18_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SUB_MACHINEGUN).damage(35).range(36).accuracy(67.5F).stability(95.4F).speed(11.76F).maxAmmo(30).mobility(90.1F + 5.5F).reloadTime(1.75F)
            .shootSound(AVASounds.MK18_SHOOT).reloadSound(AVASounds.MK18_RELOAD).silenced();

    public static Item MK18 = null;
    public static Item MK18_AIR_WARFARE = null;
    public static Item MK18_KUYO_MON = null;

    //shotguns -> .initialAccuracy(-100.0F).speed(gunProperties.getSpeed() * 2.0F).damage(gunProperties.getDamage() / 2.5F).range(gunProperties.getRange() * 1.5F)
    public static Item REMINGTON870_AMMO = null;
    private static final AVAItemGun.Properties REMINGTON870_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SHOTGUN).damage(40.5F / 2.5F).range((int) (30 * 1.5F)).accuracy(65.3F).stability(21.5F).speed(1 * 2.0F).maxAmmo(7).mobility(81.5F + 5.5F).reloadTime(0.75F)
            .shootSound(AVASounds.REMINGTON870_SHOOT).reloadSound(AVASounds.REMINGTON870_RELOAD).initialAccuracy(-100);

    public static Item REMINGTON870 = null;
    public static Item REMINGTON870_DREAMCATCHER = null;

    public static Item MP5K_MAGAZINE = null;
    private static final AVAItemGun.Properties MP5K_P = new AVAItemGun.Properties(AVAWeaponUtil.Classification.SUB_MACHINEGUN).damage(34.5F).range(44).accuracy(81.5F).stability(90.6F).speed(13.33F).maxAmmo(30).mobility(91.9F + 5.5F).reloadTime(1.75F)
            .shootSound(AVASounds.MP5K_SHOOT).reloadSound(AVASounds.MP5K_RELOAD).scopeType(AVAItemGun.ScopeTypes.X95R);

    public static Item MP5K = null;
    public static Item MP5K_FROST = null;

    public static ArrayList<Item> ITEM_SUBMACHINE_GUNS = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                X95R_MAGAZINE = new Ammo(new Item.Properties().maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("x95r_magazine"),
                X95R = new AVAItemGun(X95R_P.magazineType(X95R_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 10).addItem(Items.SAND, 12).addItem(Items.GLASS_PANE)).setRegistryName("x95r"),

                MP5SD5_MAGAZINE = new Ammo(new Item.Properties().maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("mp5sd5_magazine"),
                MP5SD5 = new AVAItemGun(MP5SD5_P.magazineType(MP5SD5_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 13).addItem(Items.FEATHER, 3).addItem(Items.CLAY_BALL, 3)).setRegistryName("mp5sd5"),

                MK18_MAGAZINE = new Ammo(new Item.Properties().maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("mk18_magazine"),
                MK18 = new AVAItemGun(MK18_P.magazineType(MK18_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 15).addItem(Items.CLAY_BALL, 3)).setRegistryName("mk18"),
                MK18_AIR_WARFARE = new AVAItemGun(MK18_P.skinned((AVAItemGun) MK18), new Recipe().addItem(MK18).mergeIngredients(SharedRecipes.AIR_WARFARE)).setRegistryName("mk18_air_warfare"),
                MK18_KUYO_MON = new AVAItemGun(MK18_P.skinned((AVAItemGun) MK18), new Recipe().addItem(MK18).mergeIngredients(SharedRecipes.KUYO_MON)).setRegistryName("mk18_kuyo_mon"),

                //shotguns -> .initialAccuracy(-10.0F).speed(gunProperties.getSpeed() * 2.0F).damage(gunProperties.getDamage() / 1.4F)
                REMINGTON870_AMMO = new Ammo(new Item.Properties(), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT, 2).addItem(Items.IRON_NUGGET, 5).setResultCount(14), false).setRegistryName("remington870_ammo"),
                REMINGTON870 = new AVAShotgun(REMINGTON870_P.magazineType(REMINGTON870_AMMO), new Recipe().addItem(Items.IRON_INGOT, 12).addItem(Items.IRON_NUGGET, 4), 8).setRegistryName("remington870"),
                REMINGTON870_DREAMCATCHER = new AVAShotgun(REMINGTON870_P.skinned((AVAItemGun) REMINGTON870), new Recipe().addItem(REMINGTON870).mergeIngredients(SharedRecipes.DREAMCATCHER), 8).setRegistryName("remington870_dreamcatcher"),

                MP5K_MAGAZINE = new Ammo(new Item.Properties().maxDamage(30), new Recipe().addItem(Items.GUNPOWDER).addItem(Items.IRON_INGOT), true).setRegistryName("mp5k_magazine"),
                MP5K = new AVAItemGun(MP5K_P.magazineType(MP5K_MAGAZINE), new Recipe().addItem(Items.IRON_INGOT, 12).addItem(Items.CLAY_BALL, 2).addItem(Items.COAL, 5)).setRegistryName("mp5k"),
                MP5K_FROST = new AVAItemGun(MP5K_P.skinned((AVAItemGun) MP5K), new Recipe().addItem(MP5K).mergeIngredients(SharedRecipes.FROST)).setRegistryName("mp5k_frost")

                );
        DistExecutor.runWhenOn(Dist.CLIENT, () -> SubmachineGuns::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((AVAItemGun) X95R).setCustomModel((origin) -> new Model(origin, X95RModel::new));
        ((AVAItemGun) MP5SD5).setCustomModel((origin) -> new Model(origin, Mp5sd5Model::new));
        ((AVAItemGun) MK18).setCustomModel((origin) -> new Model(origin, Mk18Model::new));
        ((AVAItemGun) MK18_AIR_WARFARE).setCustomModel((origin) -> new Model(origin, Mk18AirWarfareModel::new));
        ((AVAItemGun) MK18_KUYO_MON).setCustomModel((origin) -> new Model(origin, Mk18KuyomonModel::new));
        ((AVAItemGun) REMINGTON870).setCustomModel((origin) -> new Model(origin, Reminton870Model::new));
        ((AVAItemGun) REMINGTON870_DREAMCATCHER).setCustomModel((origin) -> new Model(origin, Reminton870DreamcatcherModel::new));
        ((AVAItemGun) MP5K).setCustomModel((origin) -> new Model(origin, Mp5kModel::new));
        ((AVAItemGun) MP5K_FROST).setCustomModel((origin) -> new Model(origin, Mp5kFrostModel::new));
    }
}
