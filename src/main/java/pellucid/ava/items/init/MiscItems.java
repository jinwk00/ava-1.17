package pellucid.ava.items.init;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.miscs.*;
import pellucid.ava.misc.AVAItemGroups;

import java.util.ArrayList;

public class MiscItems
{
    public static Item EU_STANDARD_BOOTS = null;
    public static Item EU_STANDARD_TROUSERS = null;
    public static Item EU_STANDARD_KEVLAR = null;
    public static Item EU_STANDARD_HELMET = null;

    public static Item NRF_STANDARD_BOOTS = null;
    public static Item NRF_STANDARD_TROUSERS = null;
    public static Item NRF_STANDARD_KEVLAR = null;
    public static Item NRF_STANDARD_HELMET = null;

    public static Item BINOCULAR = null;
    public static Item TEST_ITEM = null;
    public static Item AMMO_KIT = null;
    public static Item AMMO_KIT_II = null;

    public static final ArrayList<Item> ITEM_MISCS = new ArrayList<>();

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        registry.registerAll(
                EU_STANDARD_BOOTS = new Armours(Armours.AVAArmourMaterial.EU_STANDARD, EquipmentSlotType.FEET, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.YELLOW_DYE).addItem(Items.LEATHER_BOOTS)).setRegistryName("eu_standard_boots"),
                EU_STANDARD_TROUSERS = new Armours(Armours.AVAArmourMaterial.EU_STANDARD, EquipmentSlotType.LEGS, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.YELLOW_DYE).addItem(Items.LEATHER_LEGGINGS)).setRegistryName("eu_standard_trousers"),
                EU_STANDARD_KEVLAR = new Armours(Armours.AVAArmourMaterial.EU_STANDARD, EquipmentSlotType.CHEST, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.YELLOW_DYE).addItem(Items.LEATHER_CHESTPLATE)).setRegistryName("eu_standard_kevlar"),
                EU_STANDARD_HELMET = new Armours(Armours.AVAArmourMaterial.EU_STANDARD, EquipmentSlotType.HEAD, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.YELLOW_DYE).addItem(Items.LEATHER_HELMET)).setRegistryName("eu_standard_helmet"),

                NRF_STANDARD_BOOTS = new Armours(Armours.AVAArmourMaterial.NRF_STANDARD, EquipmentSlotType.FEET, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.BLUE_DYE).addItem(Items.LEATHER_BOOTS)).setRegistryName("nrf_standard_boots"),
                NRF_STANDARD_TROUSERS = new Armours(Armours.AVAArmourMaterial.NRF_STANDARD, EquipmentSlotType.LEGS, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.BLUE_DYE).addItem(Items.LEATHER_LEGGINGS)).setRegistryName("nrf_standard_trousers"),
                NRF_STANDARD_KEVLAR = new Armours(Armours.AVAArmourMaterial.NRF_STANDARD, EquipmentSlotType.CHEST, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.BLUE_DYE).addItem(Items.LEATHER_CHESTPLATE)).setRegistryName("nrf_standard_kevlar"),
                NRF_STANDARD_HELMET = new Armours(Armours.AVAArmourMaterial.NRF_STANDARD, EquipmentSlotType.HEAD, new Item.Properties().group(AVAItemGroups.MAIN), new Recipe().addItem(Items.IRON_INGOT, 3).addItem(Items.BLUE_DYE).addItem(Items.LEATHER_HELMET)).setRegistryName("nrf_standard_helmet"),
                BINOCULAR = new Binocular().setRegistryName("binocular"),
                TEST_ITEM = new TestItem().setRegistryName("test_item"),
                AMMO_KIT = new AmmoKit(true).setRegistryName("ammo_kit"),
                AMMO_KIT_II = new AmmoKit(false).setRegistryName("ammo_kit_ii")
        );
        ITEM_MISCS.add(EU_STANDARD_BOOTS);
        ITEM_MISCS.add(EU_STANDARD_TROUSERS);
        ITEM_MISCS.add(EU_STANDARD_KEVLAR);
        ITEM_MISCS.add(EU_STANDARD_HELMET);

        ITEM_MISCS.add(NRF_STANDARD_BOOTS);
        ITEM_MISCS.add(NRF_STANDARD_TROUSERS);
        ITEM_MISCS.add(NRF_STANDARD_KEVLAR);
        ITEM_MISCS.add(NRF_STANDARD_HELMET);

        ITEM_MISCS.add(TEST_ITEM);
        ITEM_MISCS.add(BINOCULAR);
        ITEM_MISCS.add(AMMO_KIT);
        ITEM_MISCS.add(AMMO_KIT_II);
    }
}
