package pellucid.ava.items.miscs;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import pellucid.ava.entities.scanhits.BinocularRTBulletEntity;
import pellucid.ava.misc.AVAItemGroups;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;

public class Binocular extends Item implements IHasRecipe, IClassification
{
    private static final Recipe RECIPE = new Recipe().addItem(Items.IRON_INGOT, 5).addItem(Items.PURPLE_STAINED_GLASS, 3).addItem(Items.EMERALD, 2)
            .addDescription("binocular_1").addDescription("binocular_2").addDescription("binocular_3");
    private final Multimap<String, AttributeModifier> attributeModifiers;

    public Binocular()
    {
        super(new Item.Properties().maxStackSize(1).maxDamage(60).group(AVAItemGroups.MAIN));
        ImmutableMultimap.Builder<String, AttributeModifier> map = ImmutableMultimap.builder();
        map.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADDITION));
        this.attributeModifiers = map.build();
    }

    public void fire(World world, PlayerEntity player, ItemStack stack)
    {
        if (firable(player, stack))
        {
            world.addEntity(new BinocularRTBulletEntity(world, player, false));
            player.getEntityWorld().playSound(null, player.getPosX(), player.getPosY(), player.getPosZ(), AVASounds.UAV_CAPTURES, SoundCategory.PLAYERS, 0.75F, 1.1F);
            if (!player.isCreative())
            {
                initTags(stack).putInt("ticks", 60);
                stack.setDamage(stack.getMaxDamage());
            }
        }
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (!(entityIn instanceof PlayerEntity) || !entityIn.isAlive())
            return;
        CompoundNBT compound = initTags(stack);
        if (compound.getInt("ticks") > 0)
        {
            compound.putInt("ticks", compound.getInt("ticks") - 1);
            stack.setDamage(compound.getInt("ticks"));
        }
        if (!isSelected)
            compound.putBoolean("aiming", false);
        stack.setTag(compound);
    }

    public boolean firable(PlayerEntity player, ItemStack stack)
    {
        return AVAWeaponUtil.Classification.SPECIAL_WEAPON.validate(player) && (player.abilities.isCreativeMode || initTags(stack).getInt("ticks") == 0) && initTags(stack).getBoolean("aiming");
    }

    public CompoundNBT initTags(ItemStack stack)
    {
        CompoundNBT compound;
        if (stack.hasTag())
            compound = stack.getTag();
        else
        {
            compound = new CompoundNBT();;
            compound.putInt("ticks", 0);
            compound.putBoolean("aiming", false);
        }
        return compound;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 72000;
    }

    @Override
    public UseAction getUseAction(ItemStack stack)
    {
        return UseAction.NONE;
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity)
    {
        return true;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, PlayerEntity player, Entity entity)
    {
        return true;
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged)
    {
        return oldStack.getItem() != newStack.getItem();
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EquipmentSlotType slot, ItemStack stack)
    {
        return slot == EquipmentSlotType.MAINHAND ? this.attributeModifiers : super.getAttributeModifiers(slot, stack);
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player)
    {
        return false;
    }

    @Override
    public Recipe getRecipe()
    {
        return RECIPE;
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return AVAWeaponUtil.Classification.SPECIAL_WEAPON;
    }
}
