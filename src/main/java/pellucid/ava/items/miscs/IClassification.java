package pellucid.ava.items.miscs;

import pellucid.ava.misc.AVAWeaponUtil;

public interface IClassification
{
    AVAWeaponUtil.Classification getClassification();
}
