package pellucid.ava.items.miscs;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import pellucid.ava.items.init.*;

import java.util.ArrayList;
import java.util.List;

public class TestItem extends Item
{
    public TestItem()
    {
        super(new Item.Properties());
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand handIn)
    {
        if (!worldIn.isRemote())
        {
//            PlayerInventory inv = player.inventory;
//            for (int i=0;i<inv.getSizeInventory();i++)
//                System.out.println(i + " " + inv.getStackInSlot(i));
            List<Item> items = new ArrayList<>(Pistols.ITEM_PISTOLS);
            items.addAll(Rifles.ITEM_RIFLES);
            items.addAll(Snipers.ITEM_SNIPERS);
            items.addAll(SubmachineGuns.ITEM_SUBMACHINE_GUNS);
            items.addAll(SpecialWeapons.ITEM_SPECIAL_WEAPONS);
            items.addAll(Projectiles.ITEM_PROJECTILES);
            List<String> strs = new ArrayList<>();
            for (Item item : items)
                strs.add(item.getRegistryName().getPath());
            System.out.println(strs.toString());
        }
        return super.onItemRightClick(worldIn, player, handIn);
    }
}
