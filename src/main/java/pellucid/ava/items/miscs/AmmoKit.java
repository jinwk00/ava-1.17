package pellucid.ava.items.miscs;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Rarity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import pellucid.ava.misc.AVAItemGroups;

import java.util.List;

public class AmmoKit extends Item
{
    public final boolean refillRequired;

    public AmmoKit(boolean refillRequired)
    {
        super(new Item.Properties().maxStackSize(1).rarity(Rarity.EPIC).group(AVAItemGroups.MAP_CREATION).maxDamage(200));
        this.refillRequired = refillRequired;
    }

    @Override
    public boolean hasEffect(ItemStack stack)
    {
        return !refillRequired;
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (worldIn.isRemote || !refillRequired)
            return;
        if (stack.getDamage() > 0 && worldIn.getGameTime() % 4L == 0)
            stack.setDamage(stack.getDamage() - 1);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
    {
        tooltip.add(new StringTextComponent("Ammo provided by this item is likely applicable for all weapons"));
    }
}
