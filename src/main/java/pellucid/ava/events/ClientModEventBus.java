package pellucid.ava.events;

import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.AVA;
import pellucid.ava.blocks.colouring_table.GunColouringGUI;
import pellucid.ava.blocks.crafting_table.GunCraftingGUI;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.kits.KitEntity;
import pellucid.ava.entities.kits.renderers.KitRenderer;
import pellucid.ava.entities.livings.renderers.GuardRenderer;
import pellucid.ava.entities.scanhits.renderers.EmptyRenderer;
import pellucid.ava.entities.shootables.M202RocketEntity;
import pellucid.ava.entities.shootables.renderers.M202RocketModel;
import pellucid.ava.entities.throwables.GrenadeEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.entities.throwables.ToxicSmokeGrenadeEntity;
import pellucid.ava.entities.throwables.renderers.*;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientModEventBus
{
    private static final ResourceLocation M67_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/m67.png");
    private static final ResourceLocation MK3A2_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/mk3a2.png");
    private static final ResourceLocation M116A1_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/m116a1.png");
    private static final ResourceLocation ROCKET_TEXTURE = new ResourceLocation(AVA.MODID, "textures/item/gun_textures/palette_forest.png");
    private static final ResourceLocation GRENADE_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/gm94_grenade.png");

    private static final String KEY_BINDING_CATEGORY = "Alliance of Valiant Arms Control";
    public static final KeyBinding RELOAD = new KeyBinding(new TranslationTextComponent("ava.keybindings.reload").getString(), GLFW.GLFW_KEY_R, KEY_BINDING_CATEGORY);
    public static final KeyBinding NIGHT_VISION_DEVICE_SWITCH = new KeyBinding(new TranslationTextComponent("ava.keybindings.night_vision_device_switch").getString(), GLFW.GLFW_KEY_N, KEY_BINDING_CATEGORY);

    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event)
    {
        DeferredWorkQueue.runLater(() -> {
            ScreenManager.registerFactory(CommonModEventBus.GUN_CRAFTING_TABLE_CONTAINER, GunCraftingGUI::new);
            ScreenManager.registerFactory(CommonModEventBus.GUN_COLOURING_TABLE_CONTAINER, GunColouringGUI::new);
        });
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.BULLET, EmptyRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.BINOCULAR_BULLET, EmptyRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.MELEE_RAYTRACING, EmptyRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends HandGrenadeEntity>) AVAEntities.M67, (manager) -> new ProjectileRenderer<HandGrenadeEntity>(manager, new M67Model(), M67_TEXTURE));
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends HandGrenadeEntity>) AVAEntities.MK3A2, (manager) -> new ProjectileRenderer<HandGrenadeEntity>(manager, new CylinderGrenadeModel(), MK3A2_TEXTURE));
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends HandGrenadeEntity>) AVAEntities.M116A1, (manager) -> new ProjectileRenderer<HandGrenadeEntity>(manager, new CylinderGrenadeModel(), M116A1_TEXTURE));
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends SmokeGrenadeEntity>) AVAEntities.M18, M18Renderer::new);
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends ToxicSmokeGrenadeEntity>) AVAEntities.M18_TOXIC, M18Renderer::new);
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends KitEntity>) AVAEntities.AMMO_KIT, KitRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends KitEntity>) AVAEntities.FIRST_AID_KIT, KitRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends M202RocketEntity>) AVAEntities.ROCKET, (manager) -> new ProjectileRenderer<HandGrenadeEntity>(manager, new M202RocketModel(), ROCKET_TEXTURE, (stack) -> {
            stack.scale(0.1F, 0.1F, 0.1F);
            stack.translate(0.0F, 0.25F, 0.0F);
        }));
        RenderingRegistry.registerEntityRenderingHandler((EntityType<? extends GrenadeEntity>) AVAEntities.GRENADE, (manager) -> new ProjectileRenderer<GrenadeEntity>(manager, new GM94GrenadeModel(), GRENADE_TEXTURE, (stack) -> {
            stack.translate(0.0F, -1F, 0.0F);
        }));
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.MELEE_GUARD, GuardRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.RIFLE_GUARD, GuardRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.GRENADE_LAUNCHER_GUARD, GuardRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.PISTOL_GUARD, GuardRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(AVAEntities.TOXIC_SMOKE_GUARD, GuardRenderer::new);
        ClientRegistry.registerKeyBinding(RELOAD);
        ClientRegistry.registerKeyBinding(NIGHT_VISION_DEVICE_SWITCH);
    }
}
