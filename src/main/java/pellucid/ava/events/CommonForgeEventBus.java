package pellucid.ava.events;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import pellucid.ava.AVA;
import pellucid.ava.entities.kits.AmmoKitEntity;
import pellucid.ava.entities.kits.FirstAidKitEntity;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.commands.AVACommands;

@Mod.EventBusSubscriber(modid = AVA.MODID)
public class CommonForgeEventBus extends AVACommonEvent
{
    @SubscribeEvent
    public static void onLivingDeath(LivingDeathEvent event)
    {
        LivingEntity entity = event.getEntityLiving();
        if (entity instanceof IMob)
        {
            World world = entity.getEntityWorld();
            if (!world.isRemote())
                if (RAND.nextFloat() <= WorldData.getCap(world).getMobDropsKitsChance())
                    world.addEntity(RAND.nextBoolean() ? new AmmoKitEntity(world, entity.getPosX(), entity.getPosY(), entity.getPosZ()) : new FirstAidKitEntity(world, entity.getPosX(), entity.getPosY(), entity.getPosZ()));
        }
    }

    @SubscribeEvent
    public static void onServerStarting(final FMLServerStartingEvent event)
    {
        AVACommands.registerAll(event.getCommandDispatcher());
    }
}
