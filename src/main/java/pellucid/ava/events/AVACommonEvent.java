package pellucid.ava.events;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.DamageSource;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Armours;
import pellucid.ava.misc.AVADamageSource;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.Objects;
import java.util.Random;
import java.util.UUID;

public class AVACommonEvent
{
    protected static final Random RAND = new Random();
    protected static final UUID HEALTH_BOOST_MODIFIER_ID = UUID.fromString("da805c8e-28be-40aa-a6d3-7d1df657172f");
    protected static final AttributeModifier KNOCKBACK_RESISTANCE_MODIFIER = new AttributeModifier(UUID.fromString("b7f5837a-3ddf-4384-8557-e9728c620ae6"), "Knockback Resistance Modifier", 100.0F, AttributeModifier.Operation.ADDITION);

    protected static ItemStack getHeldStack(PlayerEntity player)
    {
        return player.getHeldItemMainhand();
    }

    protected static AVAItemGun getGun(PlayerEntity player)
    {
        return (AVAItemGun) getHeldStack(player).getItem();
    }

    protected static boolean isAvailable(PlayerEntity player)
    {
        return (player != null && player.isAlive() && getHeldStack(player).getItem() instanceof AVAItemGun) && !player.isSpectator();
    }

    protected static IPlayerAction cap(PlayerEntity player)
    {
        return PlayerAction.getCap(player);
    }

    public static boolean isFullEquipped(PlayerEntity player)
    {
        Item item = player.getItemStackFromSlot(EquipmentSlotType.HEAD).getItem();
        Item item2 = player.getItemStackFromSlot(EquipmentSlotType.CHEST).getItem();
        Item item3 = player.getItemStackFromSlot(EquipmentSlotType.LEGS).getItem();
        Item item4 = player.getItemStackFromSlot(EquipmentSlotType.FEET).getItem();
        return item instanceof Armours && item2 instanceof Armours && item3 instanceof Armours && item4 instanceof Armours;
    }

    public static boolean isMovingOnGroundServer(PlayerEntity player)
    {
        CompoundNBT compound = player.getPersistentData();
        double x = Math.abs(compound.getDouble("lastposx") - player.getPosX());
        double y = Math.abs(compound.getDouble("lastposy") - player.getPosY());
        double z = Math.abs(compound.getDouble("lastposz") - player.getPosZ());
        return (x > 0.02F || y > 0.02F || z > 0.02F) && player.onGround && !player.isPassenger();
    }

    public static boolean isMovingOnGroundClient(PlayerEntity player)
    {
        return (player.getPosX() != player.lastTickPosX || player.getPosZ() != player.lastTickPosZ) && player.onGround && !player.isSwimming() && !player.isInWater();
    }

    public static boolean isAVADamageSource(DamageSource source)
    {
        return AVADamageSource.isAVADamageSource(source);
    }

    protected static boolean hasHealthBoostModifier(PlayerEntity player)
    {
        return player.getAttribute(SharedMonsterAttributes.MAX_HEALTH).getModifier(HEALTH_BOOST_MODIFIER_ID) != null;
    }

    protected static AttributeModifier getHealthBoostModifier(PlayerEntity player)
    {
        IAttributeInstance attribute = player.getAttribute(SharedMonsterAttributes.MAX_HEALTH);
        return attribute.getModifier(HEALTH_BOOST_MODIFIER_ID);
    }

    protected static void removeHealthBoostModifier(PlayerEntity player)
    {
        Objects.requireNonNull(player.getAttribute(SharedMonsterAttributes.MAX_HEALTH)).removeModifier(HEALTH_BOOST_MODIFIER_ID);
    }

    protected static void applyHealthBoostModifier(PlayerEntity player, AttributeModifier modifier)
    {
        player.getAttribute(SharedMonsterAttributes.MAX_HEALTH).applyModifier(modifier);
    }

    protected static AttributeModifier createHealthBoostModifier(int amount)
    {
        return new AttributeModifier(HEALTH_BOOST_MODIFIER_ID, () -> "health_boost", amount, AttributeModifier.Operation.ADDITION);
    }
}