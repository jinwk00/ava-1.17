package pellucid.ava.blocks.boost_block;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.events.CommonModEventBus;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class BoostTileEntity extends TileEntity implements ITickableTileEntity
{
    private boolean isAttackDamageBoost;
    private final HashMap<UUID, AtomicInteger> playerTriggerCD = new HashMap<>();

    public BoostTileEntity()
    {
        this(true);
    }

    public BoostTileEntity(boolean isAttackDamageBoost)
    {
        super(CommonModEventBus.BOOST_TE);
        this.isAttackDamageBoost = isAttackDamageBoost;
    }

    @Override
    public void tick()
    {
        if (world != null && !world.isRemote())
        {
            playerTriggerCD.forEach((id, cd) -> cd.getAndDecrement());
            playerTriggerCD.entrySet().removeIf((entry) -> entry.getValue().get() < 1);
            for (ServerPlayerEntity player : world.getEntitiesWithinAABB(ServerPlayerEntity.class, new AxisAlignedBB(pos).expand(0.0F, 0.5F, 0.0F), (player) -> !playerTriggerCD.containsKey(player.getUniqueID())))
            {
                if (player.experienceLevel >= 1)
                {
                    IPlayerAction cap = PlayerAction.getCap(player);
                    IParticleData particle;
                    String type;
                    TextFormatting colour;
                    boolean succeed = true;
                    int level;
                    if (isAttackDamageBoost)
                    {
                        int boost = cap.getAttackDamageBoost();
                        if (boost > 19)
                            succeed = false;
                        else
                            cap.setAttackDamageBoost(cap.getAttackDamageBoost() + 1);
                        particle = ParticleTypes.CRIT;
                        type = "attack damage";
                        colour = TextFormatting.RED;
                        level = cap.getAttackDamageBoost();
                    }
                    else
                    {
                        int boost = cap.getHealthBoost();
                        if (boost > 19)
                            succeed = false;
                        else
                            cap.setHealthBoost(cap.getHealthBoost() + 1);
                        particle = ParticleTypes.HAPPY_VILLAGER;
                        type = "health";
                        colour = TextFormatting.GREEN;
                        level = cap.getHealthBoost();
                    }
                    if (succeed)
                    {
                        playerTriggerCD.put(player.getUniqueID(), new AtomicInteger(25));
                        player.addExperienceLevel(-1);
                        player.sendMessage(new StringTextComponent("your " + type + " boost is increased by 1, currently on level " + level).setStyle(new Style().setColor(colour)), ChatType.CHAT);
                        if (world instanceof ServerWorld)
                        {
                            Random rand = new Random();
                            for (int i = 0; i < 15; i++)
                                ((ServerWorld) world).spawnParticle(particle, player.getPosX() + rand.nextFloat(), player.getPosY() + rand.nextFloat() * 2, player.getPosZ() + rand.nextFloat(), 1, 0.0F, 0.0F, 0.0F, 0.0F);
                        }
                        world.playSound(null, pos.getX(), pos.getY(), pos.getZ(), AVASounds.BLOCK_BOOSTS_PLAYER, SoundCategory.BLOCKS, 1.0F, 1.0F);
                    }
                }
            }
        }
    }

    @Override
    public CompoundNBT write(CompoundNBT compound)
    {
        compound.putBoolean("isattackboost", isAttackDamageBoost);
        int i = 0;
        for (UUID uuid : playerTriggerCD.keySet())
            compound.putString("uuid" + i++, uuid.toString());
        i = 0;
        for (AtomicInteger value : playerTriggerCD.values())
            compound.putInt("cd" + i++, value.get());
        compound.putInt("count", i + 1);
        return super.write(compound);
    }

    @Override
    public void read(CompoundNBT nbt)
    {
        isAttackDamageBoost = nbt.getBoolean("isattackboost");
        int count = nbt.getInt("count");
        for (int i=0;i<count;i++)
            playerTriggerCD.put(UUID.fromString(nbt.getString("uuid" + i)), new AtomicInteger(nbt.getInt("cd" + i)));
        super.read(nbt);
    }
}
