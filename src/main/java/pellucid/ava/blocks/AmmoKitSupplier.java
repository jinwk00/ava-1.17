package pellucid.ava.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.misc.AVASounds;

import java.util.Random;

public class AmmoKitSupplier extends HorizontalBlock
{
    public static final VoxelShape NEGATIVE_Z = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 10.0D);
    public static final VoxelShape POSITIVE_Z = Block.makeCuboidShape(0.0D, 0.0D, 6.0D, 16.0D, 16.0D, 16.0D);
    public static final VoxelShape NEGATIVE_X = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 10.0D, 16.0D, 16.0D);
    public static final VoxelShape POSITIVE_X = Block.makeCuboidShape(6.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D);
    public static final BooleanProperty ACTIVE = BooleanProperty.create("active");

    public AmmoKitSupplier()
    {
        super(Block.Properties.create(Material.IRON).doesNotBlockMovement().hardnessAndResistance(-1.0F, 3600000.0F).noDrops());
        setDefaultState(getDefaultState()
                .with(HORIZONTAL_FACING, Direction.NORTH)
                .with(ACTIVE, true));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(HORIZONTAL_FACING, ACTIVE);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context)
    {
        return this.getDefaultState()
                .with(HORIZONTAL_FACING, context.getPlacementHorizontalFacing());
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return this.getShape(state, worldIn, pos, context);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        switch (state.get(HORIZONTAL_FACING))
        {
            case NORTH:
            default:
                return NEGATIVE_Z;
            case SOUTH:
                return POSITIVE_Z;
            case WEST:
                return NEGATIVE_X;
            case EAST:
                return POSITIVE_X;
        }
    }

    @Override
    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn)
    {
        if (!(entityIn instanceof PlayerEntity) || worldIn.isRemote())
            return;
        PlayerEntity player = (PlayerEntity) entityIn;
        ItemStack stack = player.getHeldItemMainhand();
        if (stack.getItem() instanceof AVAItemGun)
        {
            Item ammo = ((AVAItemGun) stack.getItem()).getMagazineType();
            if (ammo instanceof Ammo && state.get(ACTIVE))
            {
                ((Ammo) ammo).addToInventory(player, 3);
                worldIn.setBlockState(pos, getDefaultState().with(HORIZONTAL_FACING, state.get(HORIZONTAL_FACING)).with(ACTIVE, false));
                worldIn.playSound(null, pos.getX(), pos.getY(), pos.getZ(), AVASounds.AMMO_SUPPLIER_CONSUME, SoundCategory.BLOCKS, 1.0F, 1.0F);
            }
            worldIn.getPendingBlockTicks().scheduleTick(pos, this, 40, TickPriority.VERY_LOW);
        }
    }

    @Override
    public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
    {
        worldIn.setBlockState(pos, getDefaultState().with(HORIZONTAL_FACING, state.get(HORIZONTAL_FACING)).with(ACTIVE, true));
    }
}
