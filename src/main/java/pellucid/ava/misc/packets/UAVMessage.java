package pellucid.ava.misc.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.function.Supplier;

public class UAVMessage
{
    private int id;
    private int uav;

    public UAVMessage(int id, int uav)
    {
        this.id = id;
        this.uav = uav;
    }

    public static void encode(UAVMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeInt(msg.id).writeInt(msg.uav);
    }

    public static UAVMessage decode(PacketBuffer packetBuffer)
    {
        return new UAVMessage(packetBuffer.readInt(), packetBuffer.readInt());
    }

    public static void handle(UAVMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> run(message)));
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    public static void run(UAVMessage message)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (minecraft.world == null || player == null)
            return;
        Entity entity = minecraft.world.getEntityByID(message.id);
        if (entity instanceof PlayerEntity)
            PlayerAction.getCap((PlayerEntity) entity).setUAVDuration(null, message.uav);
    }
}
