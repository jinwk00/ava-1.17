package pellucid.ava.misc.packets;

import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.IHasRecipe;
import pellucid.ava.items.miscs.Recipe;

import java.util.function.Supplier;

public class PaintMessage
{
    private Item from;
    private Item to;

    public PaintMessage(Item from, Item to)
    {
        this.from = from;
        this.to = to;
    }

    public static void encode(PaintMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeItemStack(new ItemStack(msg.from)).writeItemStack(new ItemStack(msg.to));
    }

    public static PaintMessage decode(PacketBuffer packetBuffer)
    {
        return new PaintMessage(packetBuffer.readItemStack().getItem(), packetBuffer.readItemStack().getItem());
    }

    public static void handle(PaintMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
        {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                if (message.to instanceof IHasRecipe && message.to.getItem() instanceof AVAItemGun && !player.isCreative())
                {
                    if (((AVAItemGun) message.to.getItem()).isMaster())
                    {
                        if (remove(player.inventory, message.from.getItem()))
                            add(player, message.to);
                    }
                    else
                    {
                        Recipe recipe = ((IHasRecipe) message.to).getRecipe();
                        if (recipe.canCraft(player, message.to))
                            recipe.craft(player, message.to);
                    }
                }
                else if (player.isCreative())
                    add(player, message.to);
            }
        });
        ctx.get().setPacketHandled(true);
    }

    private static void add(ServerPlayerEntity player, Item to)
    {
        ItemStack result = new ItemStack(to);
        result.setDamage(result.getMaxDamage());
        if (!player.inventory.addItemStackToInventory(result))
            player.world.addEntity(new ItemEntity(player.world, player.getPosX(), player.getPosY(), player.getPosZ(), result));
    }

    private static boolean remove(PlayerInventory inventory, Item item)
    {
        for (int slot=0;slot<inventory.getSizeInventory();slot++)
            if (inventory.getStackInSlot(slot).getItem() == item)
            {
                inventory.getStackInSlot(slot).shrink(1);
                return true;
            }
        return false;
    }
}
