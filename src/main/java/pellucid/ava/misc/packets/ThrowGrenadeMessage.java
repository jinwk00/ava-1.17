package pellucid.ava.misc.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import pellucid.ava.items.throwables.ThrowableItem;

import java.util.function.Supplier;

public class ThrowGrenadeMessage
{
    private int phase;

    public ThrowGrenadeMessage(int phase)
    {
        this.phase = phase;
    }

    public static void encode(ThrowGrenadeMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeInt(msg.phase);
    }

    public static ThrowGrenadeMessage decode(PacketBuffer packetBuffer)
    {
        return new ThrowGrenadeMessage(packetBuffer.readInt());
    }

    public static void handle(ThrowGrenadeMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if (!(stack.getItem() instanceof ThrowableItem))
                {
                    ctx.get().setPacketHandled(true);
                    return;
                }
                ThrowableItem grenade = (ThrowableItem) stack.getItem();
                if (message.phase == 2)
                    grenade.hold(player.getServerWorld(), player, stack);
                else
                    grenade.toss(player.getServerWorld(), player, stack, message.phase == 1);
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
