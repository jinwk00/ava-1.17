package pellucid.ava.misc.packets;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVASpecialWeapon;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Armours;
import pellucid.ava.items.miscs.IClassification;
import pellucid.ava.misc.config.AVAServerConfig;

import java.util.function.Supplier;

public class PresetMessage
{
    private final String p, se, m, p1, p2, p3, sp;

    public PresetMessage(String p, String se, String m, String p1, String p2, String p3, String sp)
    {
        this.p = p;
        this.se = se;
        this.m = m;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.sp = sp;
    }

    public static void encode(PresetMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeString(msg.p);
        packetBuffer.writeString(msg.se);
        packetBuffer.writeString(msg.m);
        packetBuffer.writeString(msg.p1);
        packetBuffer.writeString(msg.p2);
        packetBuffer.writeString(msg.p3);
        packetBuffer.writeString(msg.sp);
    }

    public static PresetMessage decode(PacketBuffer pkt)
    {
        return new PresetMessage(
                pkt.readString(32767),
                pkt.readString(32767),
                pkt.readString(32767),
                pkt.readString(32767),
                pkt.readString(32767),
                pkt.readString(32767),
                pkt.readString(32767));
    }

    public static void handle(PresetMessage ms, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() ->
        {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null && AVAServerConfig.isCompetitiveModeActivated())
            {
                player.inventory.clear();
                giveIfExist(player, new int[]{0, 1, 2, 3, 3, 3, 4}, ms.p, ms.se, ms.m, ms.p1, ms.p2, ms.p3, ms.sp);
                if (!giveArmour(player, "eu"))
                    giveArmour(player, "nrf");
            }
        });
        ctx.get().setPacketHandled(true);
    }

    private static boolean giveArmour(PlayerEntity player, String team)
    {
        if (player.world.getScoreboard().getTeamNames().contains(team))
            if (player.getTeam() == player.world.getScoreboard().getTeam(team))
            {
                Armours.giveTo(player, team.equals("eu"));
                return true;
            }
        return false;
    }

    private static void giveIfExist(PlayerEntity player, int[] slots, String... items)
    {
        for (int i = 0; i < items.length; i++)
        {
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(items[i]));
            if (item instanceof IClassification)
            {
                if (slots[i] != ((IClassification) item).getClassification().getSlotIndex())
                    continue;
                ItemStack gunStack = new ItemStack(item);
                if (item instanceof AVASpecialWeapon)
                {
                    Item mag = ((AVAItemGun) item).getMagazineType();
                    ItemStack magStack = new ItemStack(mag);
                    magStack.setCount(((AVASpecialWeapon) item).getMaxAmmo());
                    player.addItemStackToInventory(magStack);
                    ((AVASpecialWeapon) item).forceReload(gunStack);
                }
                else if (item instanceof AVAItemGun)
                {
                    Item mag = ((AVAItemGun) item).getMagazineType();
                    if (mag instanceof Ammo)
                        ((Ammo) mag).addToInventory(player, 5);
                    else
                    {
                        ItemStack stack = new ItemStack(mag);
                        stack.setCount(5);
                        player.addItemStackToInventory(stack);
                    }
                    ((AVAItemGun) item).forceReload(gunStack);
                }
                player.addItemStackToInventory(gunStack);
            }
        }
    }
}
