package pellucid.ava.misc.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import pellucid.ava.misc.commands.RecoilRefundTypeCommand;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class WorldData implements ICapabilitySerializable<CompoundNBT>, IWorldData
{
    @CapabilityInject(IWorldData.class)
    public static Capability<IWorldData> WORLD_CAPABILITY = null;

    private final LazyOptional<IWorldData> lazyOptional = LazyOptional.of(WorldData::new);
    private boolean shouldRenderCrosshair = true;
    private boolean friendlyFire = true;
    private boolean reducedFriendlyFire = true;
    private boolean doGlassBreak = true;
    private float mobDropsKitsChance = 0.2F;
    private RecoilRefundTypeCommand.RefundType recoilRefundType = RecoilRefundTypeCommand.RefundType.EXPONENTIAL;

    public WorldData() {}

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
    {
        return cap == WORLD_CAPABILITY ? lazyOptional.cast() : LazyOptional.empty();
    }

    public static IWorldData getCap(World world)
    {
        return world.getCapability(WORLD_CAPABILITY).orElseThrow(() -> new NullPointerException("getting capability"));
    }

    @Override
    public CompoundNBT serializeNBT()
    {
        return (CompoundNBT) WORLD_CAPABILITY.getStorage().writeNBT(WORLD_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during writing Player Capability")), null);
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt)
    {
        WORLD_CAPABILITY.getStorage().readNBT(WORLD_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during reading Player Capability")), null, nbt);
    }

    @Override
    public void setShouldRenderCrosshair(boolean value)
    {
        this.shouldRenderCrosshair = value;
    }

    @Override
    public void setFriendlyFire(boolean value)
    {
        this.friendlyFire = value;
    }

    @Override
    public void setReducedFriendlyFire(boolean value)
    {
        this.reducedFriendlyFire = value;
    }

    @Override
    public void setGlassDestroyable(boolean value)
    {
        this.doGlassBreak = value;
    }

    @Override
    public void setMobDropsKitsChance(float chance)
    {
        this.mobDropsKitsChance = Math.max(0F, Math.min(1F, chance));
    }

    @Override
    public void setRecoilRefundType(RecoilRefundTypeCommand.RefundType type)
    {
        this.recoilRefundType = type;
    }

    @Override
    public boolean shouldRenderCrosshair()
    {
        return this.shouldRenderCrosshair;
    }

    @Override
    public boolean isFriendlyFireAllowed()
    {
        return this.friendlyFire;
    }

    @Override
    public boolean isFriendlyFireReduced()
    {
        return this.reducedFriendlyFire;
    }

    @Override
    public boolean isGlassDestroyable()
    {
        return this.doGlassBreak;
    }

    @Override
    public float getMobDropsKitsChance()
    {
        return this.mobDropsKitsChance;
    }

    @Override
    public RecoilRefundTypeCommand.RefundType getRecoilRefundType()
    {
        return recoilRefundType;
    }

    @Override
    public Capability.IStorage<IWorldData> getStorage()
    {
        return WORLD_CAPABILITY.getStorage();
    }

    public static class WorldDataStorage implements Capability.IStorage<IWorldData>
    {
        public WorldDataStorage(){}

        @Nullable
        @Override
        public INBT writeNBT(Capability<IWorldData> capability, IWorldData instance, Direction side)
        {
            CompoundNBT compound = new CompoundNBT();
            compound.putBoolean("crosshair", instance.shouldRenderCrosshair());
            compound.putBoolean("friendlyfire", instance.isFriendlyFireAllowed());
            compound.putBoolean("friendlyreduced", instance.isFriendlyFireReduced());
            compound.putBoolean("glassbreak", instance.isGlassDestroyable());
            compound.putFloat("dropskits", instance.getMobDropsKitsChance());
            compound.putString("recoilrefundtype", instance.getRecoilRefundType().toString());
            return compound;
        }

        @Override
        public void readNBT(Capability<IWorldData> capability, IWorldData instance, Direction side, INBT nbt)
        {
            CompoundNBT compound = (CompoundNBT) nbt;
            instance.setShouldRenderCrosshair(compound.getBoolean("crosshair"));
            instance.setFriendlyFire(compound.getBoolean("friendlyfire"));
            instance.setReducedFriendlyFire(compound.getBoolean("friendlyreduced"));
            instance.setGlassDestroyable(compound.getBoolean("glassbreak"));
            instance.setMobDropsKitsChance(compound.getFloat("dropskits"));
            instance.setRecoilRefundType(RecoilRefundTypeCommand.RefundType.valueOf(compound.getString("recoilrefundtype")));
        }
    }
}
