package pellucid.ava.misc;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.items.init.Rifles;

public class AVAItemGroups
{
    public static final ItemGroup MAIN = new ItemGroup(AVA.MODID + ":main")
    {
        @OnlyIn(Dist.CLIENT)
        public ItemStack createIcon()
        {
            return new ItemStack(Rifles.M4A1);
        }
    };

    public static final ItemGroup MAP_CREATION = new ItemGroup(AVA.MODID + ":map_creation")
    {
        @OnlyIn(Dist.CLIENT)
        public ItemStack createIcon()
        {
            return new ItemStack(AVABlocks.AMMO_KIT_SUPPLIER);
        }
    };
}
