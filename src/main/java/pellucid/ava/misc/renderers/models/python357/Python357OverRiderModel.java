package pellucid.ava.misc.renderers.models.python357;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class Python357OverRiderModel extends Python357Model
{
    public static final ModelResourceLocation WHEEL = new ModelResourceLocation("ava:python357/python357_overrider_wheel#inventory");

    public Python357OverRiderModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    protected ModelResourceLocation getWheel()
    {
        return WHEEL;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(WHEEL);
    }
}
