package pellucid.ava.misc.renderers.models.mosin_nagant;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MosinNagantModel extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(-4, 1, 0),
            new Vector3f(-5.5F, 5.5F, 3.75F),
            new Vector3f(1.75F, 1.5F, 0.95F)
    );

    private static final float[] RUN_ROTATION = new float[]{-14, 61, 36};
    private static final float[] RUN_SCALE = new float[]{1.75F, 2, 1.5F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-10.75F, 2, 1}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-11.75F, 1.5F, 1}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-13, 2, 1}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{26, -3, 13}, new float[]{-5, 8.25F, -2}, new float[]{1.75F, 2, 1.5F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RELOAD_1));
        add(new Animation(19, RELOAD_1));
    }};

    protected static final ArrayList<Animation> FIRE_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(3, RELOAD_1));
        add(new Animation(16, RELOAD_1));
        add(new Animation(19, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 45}, new float[]{-0.25F, -0.25F, 0}, new float[]{1, 1, 1});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_RELOADING_FP));
        add(new Animation(5, LEFT_HAND_RELOADING_FP));
        add(new Animation(25, LEFT_HAND_RELOADING_FP));
        add(new Animation(40, LEFT_HAND_RELOADING_FP));
    }};

    private static final Perspective RIGHT_HAND_ORIGINAL_FP = new Perspective(new float[]{0, 0, 0}, new float[]{-0.25F, -0.65F, 0.35F}, new float[]{1, 1, 1});
    private static final Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{0, 0, 0}, new float[]{0, -0.5F, 0}, new float[]{1, 1, 1});
    public static final ArrayList<Animation> RIGHT_HAND_RELOAD_ANIMATION_FP =  new ArrayList<Animation>() {{
    add(new Animation(0, RIGHT_HAND_ORIGINAL_FP));
    add(new Animation(4, RIGHT_HAND_RELOADING_FP));
    add(new Animation(19, RIGHT_HAND_ORIGINAL_FP));
}};

    public static final ModelResourceLocation BULLET = new ModelResourceLocation("ava:mosin_nagant/mosin_nagant_bullet#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:mosin_nagant/mosin_nagant_slide#inventory");

    public MosinNagantModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getBulletQuads());
        quads.addAll(getSlideQuads());
        return quads;
    }

    protected ModelResourceLocation getBullet()
    {
        return BULLET;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getSlideQuads()
    {
        return get(getSlide(), (newQuad) -> {
            if (!translateQuad(newQuad, 3.5F, reload, 1, 40, Direction.SOUTH))
                if (!translateQuadTo(newQuad, 1.5F, fire, 8, 13, Direction.SOUTH))
                    translateQuadFrom(newQuad, 1.5F, fire, 14, 40, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getBulletQuads()
    {
        return get(getBullet(), (newQuad) -> translateQuadFrom(newQuad, 1.5F, reload, 3, 10, Direction.WEST, Direction.UP), !(this.reload >= 3 && this.reload < 10));
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(BULLET);
        ModelLoader.addSpecialModel(SLIDE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 8, 3.5F);
                scale = new Vector3f(2, 2, 1.65F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                scale = v3f(0.75F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(-2.25F, 0, 0);
                scale = v3f(2.15F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type)
    {
        if (this.fire != 0)
        {
            Perspective perspective3f = getPerspectiveInBetween(FIRE_ANIMATION, this.fire);
            rotation.set(perspective3f.getRotation());
            translation.set(perspective3f.getTranslation());
            scale.set(perspective3f.getScale());
        }
        super.modifyPerspective(rotation, translation, scale, type, false);
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
