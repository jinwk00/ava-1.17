package pellucid.ava.misc.renderers.models.p226;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class P226Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, -13, 0),
            new Vector3f(-2, 3.5F, 1.5F),
            new Vector3f(1.5F, 1.5F, 1.5F)
    );

    private static final float[] RUN_ROTATION = new float[]{55, -13, 20};
    private static final float[] RUN_SCALE = fArr(1.5F);
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{0.25F, 5, 1.5F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{0.75F, 3.75F, 1.5F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{1.25F, 5, 1.5F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD = new Perspective(new float[]{31, -13, -45}, new float[]{0, 6.5F, 1.5F}, fArr(1.5F));
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(5, RELOAD));
        add(new Animation(25, RELOAD));
        add(new Animation(30, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective RIGHT_HAND_ORIGINAL_FP = new Perspective(fArr(0), new float[]{0, -0.5F, 0}, new float[]{1, 1, 1});
    private static final Perspective RIGHT_HAND_RELOADING_FP = new Perspective(new float[]{-35, 45, 0}, new float[]{-0.15F, -0.45F, -0.1F}, fArr(0.85F));
    public static final ArrayList<Animation> RIGHT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_ORIGINAL_FP));
        add(new Animation(5, RIGHT_HAND_RELOADING_FP));
        add(new Animation(25, RIGHT_HAND_RELOADING_FP));
        add(new Animation(30, RIGHT_HAND_ORIGINAL_FP));
    }};

    private static final float[] LEFT_HAND_RUN_ROTATION = new float[]{-45, 0, 30};
    private static final Perspective LEFT_HAND_RUN_FP_1 = new Perspective(LEFT_HAND_RUN_ROTATION, new float[]{-0.7F, -0.05F, 0.55F}, ONE_A);
    private static final Perspective LEFT_HAND_RUN_FP_2 = new Perspective(LEFT_HAND_RUN_ROTATION, new float[]{-0.75F, -0.05F, 0.6F}, ONE_A);
    public static final ArrayList<Animation> LEFT_HAND_RUN_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, LEFT_HAND_RUN_FP_1));
        add(new Animation(3, LEFT_HAND_RUN_FP_2));
        add(new Animation(6, new Perspective(LEFT_HAND_RUN_ROTATION, new float[]{-0.8F, -0.05F, 0.55F}, ONE_A)));
        add(new Animation(9, LEFT_HAND_RUN_FP_2));
        add(new Animation(12, LEFT_HAND_RUN_FP_1));
    }};

    private static final float[] RIGHT_HAND_RUN_ROTATION = new float[]{-50, -15, 0};
    private static final Perspective RIGHT_HAND_RUN_FP_1 = new Perspective(RIGHT_HAND_RUN_ROTATION, new float[]{-0.25F, 0.15F, 0.6F}, ONE_A);
    private static final Perspective RIGHT_HAND_RUN_FP_2 = new Perspective(RIGHT_HAND_RUN_ROTATION, new float[]{-0.3F, 0.15F, 0.65F}, ONE_A);
    public static final ArrayList<Animation> RIGHT_HAND_RUN_ANIMATION_FP = new ArrayList<Animation>() {{
        add(new Animation(0, RIGHT_HAND_RUN_FP_1));
        add(new Animation(3, RIGHT_HAND_RUN_FP_2));
        add(new Animation(6, new Perspective(RIGHT_HAND_RUN_ROTATION, new float[]{-0.35F, 0.15F, 0.6F}, ONE_A)));
        add(new Animation(9, RIGHT_HAND_RUN_FP_2));
        add(new Animation(12, RIGHT_HAND_RUN_FP_1));
    }};


    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:p226/p226_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:p226/p226_slide#inventory");
    public static final ModelResourceLocation FIRE = new ModelResourceLocation("ava:p226/p226_fire#inventory");

    public P226Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getFireQuads(state, rand));
        quads.addAll(getSlideQuads());
        quads.addAll(getMagazineQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    protected List<BakedQuad> getSlideQuads()
    {
        return get(getSlide(), (newQuad) -> {
            if (this.fire != 0 || (this.reload > 0 && this.reload < 25))
                translateQuad(newQuad, Direction.SOUTH, 2);
            translateQuadFrom(newQuad, 2.0F, reload, 25, 31, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 5.0F, reload, 5, 15, Direction.DOWN))
                translateQuadTo(newQuad, 2.0F, reload, 5, 15, Direction.SOUTH);
            else if (translateQuadFrom(newQuad, 5.0F, reload, 15, 25, Direction.UP))
                translateQuadFrom(newQuad, 2.0F, reload, 15, 25, Direction.NORTH);
        }, this.reload > 5 && this.reload < 25);
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
        ModelLoader.addSpecialModel(FIRE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = ONE_V.copy();
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(0, 1, 3);
                scale = v3f(1.65F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(-1, 1, 0);
                scale = v3f(1.25F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(-1, 1.25F, 0);
                scale = v3f(2);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
