package pellucid.ava.misc.renderers.models.fn_fnc;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class FnfncDreamcatcherModel extends FnfncModel
{
    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:fn_fnc/fn_fnc_dreamcatcher_magazine#inventory");

    public FnfncDreamcatcherModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
    }
}
