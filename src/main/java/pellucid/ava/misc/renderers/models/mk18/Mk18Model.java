package pellucid.ava.misc.renderers.models.mk18;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Mk18Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, 0, 0),
            new Vector3f(-5, 4.75F, 4.75F),
            new Vector3f(1, 1, 0.45F)
    );

    private static final float[] RUN_ROTATION = new float[]{-42, 65, 53};
    private static final float[] RUN_SCALE = new float[]{0.65F, 0.65F, 0.6F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-9, 5.75F, 8.5F}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-9.5F, 5.5F, 8.5F}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-10, 5.75F, 8.5F}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective RELOAD_1 = new Perspective(new float[]{26, 0, 0}, new float[]{-5, 6, 4}, new float[]{1, 1, 0.65F});
    private static final Perspective RELOAD_2 = new Perspective(new float[]{21, 9, -17}, new float[]{-4.5F, 6, 4}, new float[]{0.9F, 0.9F, 0.65F});
    private static final Perspective RELOAD_3 = new Perspective(new float[]{42, -33, 32}, new float[]{-3.25F, 8.5F, 3.25F}, new float[]{0.9F, 0.9F, 0.9F});
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(2, RELOAD_1));
        add(new Animation(7, RELOAD_1));
        add(new Animation(10, RELOAD_2));
        add(new Animation(16, RELOAD_2));
        add(new Animation(19, RELOAD_1));
        add(new Animation(21, RELOAD_1));
        add(new Animation(25, RELOAD_3));
        add(new Animation(32, RELOAD_3));
        add(new Animation(35, ORIGINAL_FP_RIGHT));
    }};

    private static final Perspective LEFT_HAND_ORIGINAL_FP = new Perspective(new float[]{-20, -10, 40}, new float[]{0.3F, -0.85F, -0.06F}, new float[]{0.35F, 1, 0.35F});
    private static final Perspective LEFT_HAND_RELOADING_FP = new Perspective(new float[]{-20, -10, 40}, new float[]{0.2F, -0.6F, -0.06F}, new float[]{0.35F, 1, 0.35F});
    public static final ArrayList<Animation> LEFT_HAND_RELOAD_ANIMATION_FP = new ArrayList<Animation>() {{
    add(new Animation(0, LEFT_HAND_ORIGINAL_FP));
    add(new Animation(23, LEFT_HAND_ORIGINAL_FP));
    add(new Animation(25, LEFT_HAND_RELOADING_FP));
    add(new Animation(32, LEFT_HAND_ORIGINAL_FP));
    add(new Animation(35, LEFT_HAND_ORIGINAL_FP));
}};

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:mk18/mk18_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:mk18/mk18_slide#inventory");

    public Mk18Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(getMagazineQuads());
        quads.addAll(getSlideQuads());
        return quads;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getSlideQuads()
    {
        return get(getSlide(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 1.0F, reload, 27, 31, Direction.SOUTH))
                translateQuadFrom(newQuad, 1.0F, reload, 31, 33, Direction.NORTH);
        });
    }

    protected List<BakedQuad> getMagazineQuads()
    {
        return get(getMagazine(), (newQuad) -> {
            if (!translateQuadTo(newQuad, 6.0F, reload, 3, 9, Direction.DOWN))
                if (!translateQuad(newQuad, -6.0F, reload, 9, 11, Direction.UP))
                    translateQuadFrom(newQuad, 6.0F, reload, 11, 17, Direction.UP);
        });
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2F, 4.5F, 4.25F);
                scale = v3f(1.45F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -3, 0);
                scale = v3f(0.75F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                scale = v3f(0.65F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0, 0, -1.25F);
                scale = v3f(1.3F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
