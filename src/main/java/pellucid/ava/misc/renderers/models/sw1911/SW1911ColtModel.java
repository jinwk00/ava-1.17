package pellucid.ava.misc.renderers.models.sw1911;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.models.p226.P226Model;

import javax.annotation.Nullable;

public class SW1911ColtModel extends P226Model
{

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:sw1911/sw1911_colt_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:sw1911/sw1911_colt_slide#inventory");

    public SW1911ColtModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }
}
