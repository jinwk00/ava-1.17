package pellucid.ava.misc.renderers.models.m24;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class M24Model extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(0, 5, 0),
            new Vector3f(-6.25F, 5.5F, 4.25F),
            new Vector3f(2, 2, 1));

    private static final float[] RUN_ROTATION = new float[]{-4, 73, 29};
    private static final float[] RUN_SCALE = new float[]{0.75F, 0.75F, 0.75F};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-10.5F, 6.75F, 9}, RUN_SCALE);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-10.75F, 6.5F, 9}, RUN_SCALE);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-11, 6.75F, 9}, RUN_SCALE)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    private static final Perspective FIRE_1 = new Perspective(new float[]{-2, -8, 33}, new float[]{-6.5F, 5.25F, 5.5F}, new float[]{2, 2, 1});

    private static final Perspective RELOAD_1 = new Perspective(new float[]{40, 35, -4}, new float[]{-9.5F, 6.75F, 5.25F}, new float[]{2, 2, 1});
    private static final Perspective RELOAD_2 = FIRE_1;
    protected static final ArrayList<Animation> RELOAD_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(3, RELOAD_1));
        add(new Animation(12, RELOAD_1));
        add(new Animation(15, RELOAD_2));
        add(new Animation(23, RELOAD_2));
        add(new Animation(25, ORIGINAL_FP_RIGHT));
        add(new Animation(30, ORIGINAL_FP_RIGHT));
    }};

    protected static final ArrayList<Animation> FIRE_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(3, FIRE_1));
        add(new Animation(16, FIRE_1));
        add(new Animation(19, ORIGINAL_FP_RIGHT));
    }};

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:m24/m24_magazine#inventory");
    public static final ModelResourceLocation SLIDE = new ModelResourceLocation("ava:m24/m24_slide#inventory");

    public M24Model(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
        if (side != null)
            return quads;
        quads.addAll(this.getMagazineQuads(state, rand));
        quads.addAll(this.getSlideQuads(state, rand));
        return quads;
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return null;
    }

    protected List<BakedQuad> getSlideQuads(@Nullable BlockState state, Random rand)
    {
        return get(this.getSlide(), state, rand, (newQuad) -> {
            if (!translateQuadTo(newQuad, 0.5F, fire, 5, 10, Direction.SOUTH))
                if (!translateQuadFrom(newQuad, -0.5F, fire, 10, 12, Direction.NORTH))
                    if (!translateQuadTo(newQuad, 0.5F, fire, 12, 16, Direction.SOUTH))
                        translateQuadFrom(newQuad, -0.5F, fire, 16, 20, Direction.NORTH);
        });
    }

    protected ModelResourceLocation getSlide()
    {
        return SLIDE;
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected List<BakedQuad> getMagazineQuads(@Nullable BlockState state, Random rand)
    {
        return get(getMagazine(), (newQuad) -> {
            if (translateQuadTo(newQuad, 3.0F, reload, 1, 5, Direction.DOWN))
                translateQuadTo(newQuad, 3.0F, reload, 1, 5, Direction.DOWN);
            else if (translateQuad(newQuad, -3.0F, reload, 5, 6, Direction.UP))
                translateQuad(newQuad, -1.0F, reload, 5, 6, Direction.NORTH);
            else if (translateQuadFrom(newQuad, 3.0F, reload, 6, 9, Direction.UP))
                translateQuadTo(newQuad, 1.0F, reload, 6, 9, Direction.NORTH);
        }, reload >= 9);
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(SLIDE);
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2, 9.5F, 2.5F);
                scale = new Vector3f(2.25F, 2.25F, 2);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0, 0, -90);
                translation = new Vector3f(0, -3, -2);
                scale = v3f(1.25F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(1.5F, 0, 0);
                scale = v3f(0.9F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(4, 0, 0);
                scale = v3f(2.8F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type)
    {
        if (this.fire != 0)
        {
            Perspective perspective3f = getPerspectiveInBetween(FIRE_ANIMATION, this.fire);
            rotation.set(perspective3f.getRotation());
            translation.set(perspective3f.getTranslation());
            scale.set(perspective3f.getScale());
        }
        super.modifyPerspective(rotation, translation, scale, type, false);
    }

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION;
    }
}
