package pellucid.ava.misc.renderers.models.fg42;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class FG42DreamcatcherModel extends FG42Model
{
    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:fg42/fg42_dreamcatcher_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:fg42/fg42_sumire_handle#inventory");

    public FG42DreamcatcherModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
    }
}
