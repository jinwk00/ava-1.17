package pellucid.ava.misc.renderers.models.remington870;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class Reminton870DreamcatcherModel extends Reminton870Model
{
    public static final ModelResourceLocation BULLET = new ModelResourceLocation("ava:remington870/remington870_dreamcatcher_bullet#inventory");
    public static final ModelResourceLocation FOREARM = new ModelResourceLocation("ava:remington870/remington870_dreamcatcher_forearm#inventory");

    public Reminton870DreamcatcherModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getBullet()
    {
        return BULLET;
    }

    protected ModelResourceLocation getForearm()
    {
        return FOREARM;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(BULLET);
        ModelLoader.addSpecialModel(FOREARM);
    }
}
