package pellucid.ava.misc.renderers.models.mp5k;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.world.World;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import javax.annotation.Nullable;

public class Mp5kFrostModel extends Mp5kModel
{

    public static final ModelResourceLocation MAGAZINE = new ModelResourceLocation("ava:mp5k/mp5k_frost_magazine#inventory");
    public static final ModelResourceLocation HANDLE = new ModelResourceLocation("ava:mp5k/mp5k_frost_handle#inventory");

    public Mp5kFrostModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
    }

    protected ModelResourceLocation getMagazine()
    {
        return MAGAZINE;
    }

    protected ModelResourceLocation getHandle()
    {
        return HANDLE;
    }

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(MAGAZINE);
        ModelLoader.addSpecialModel(HANDLE);
    }
}
