package pellucid.ava.misc.renderers;


import net.minecraft.client.renderer.Vector3f;

public class Animation
{
    public final int targetTicks;
    public final Perspective target3f;

    public Animation(int targetTicks, Perspective target3f)
    {
        this.targetTicks = targetTicks;
        this.target3f = target3f;
    }

    public Perspective getPerspectiveInBetween(Animation nextAnimation, int currentTicks)
    {
        Perspective t3f = nextAnimation.target3f;
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        int ticksDiff = nextAnimation.targetTicks - this.targetTicks;
        if (ticksDiff == 0)
            return nextAnimation.target3f;
        currentTicks = currentTicks - this.targetTicks;
        rotation.setX(target3f.rotation.getX() + (t3f.rotation.getX() - target3f.rotation.getX()) / ticksDiff * currentTicks);
        rotation.setY(target3f.rotation.getY() + (t3f.rotation.getY() - target3f.rotation.getY()) / ticksDiff * currentTicks);
        rotation.setZ(target3f.rotation.getZ() + (t3f.rotation.getZ() - target3f.rotation.getZ()) / ticksDiff * currentTicks);
        translation.setX(target3f.translation.getX() + (t3f.translation.getX() - target3f.translation.getX()) / ticksDiff * currentTicks);
        translation.setY(target3f.translation.getY() + (t3f.translation.getY() - target3f.translation.getY()) / ticksDiff * currentTicks);
        translation.setZ(target3f.translation.getZ() + (t3f.translation.getZ() - target3f.translation.getZ()) / ticksDiff * currentTicks);
        scale.setX(target3f.scale.getX() + (t3f.scale.getX() - target3f.scale.getX()) / ticksDiff * currentTicks);
        scale.setY(target3f.scale.getY() + (t3f.scale.getY() - target3f.scale.getY()) / ticksDiff * currentTicks);
        scale.setZ(target3f.scale.getZ() + (t3f.scale.getZ() - target3f.scale.getZ()) / ticksDiff * currentTicks);
        return new Perspective(rotation, translation, scale);
    }
}
