package pellucid.ava.misc.renderers;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import pellucid.ava.items.init.*;
import pellucid.ava.items.miscs.ICustomModel;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;
import pellucid.ava.misc.renderers.models.fg42.FG42DreamcatcherModel;
import pellucid.ava.misc.renderers.models.fg42.FG42Model;
import pellucid.ava.misc.renderers.models.fg42.FG42SumireModel;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncDreamcatcherModel;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncFullmoonModel;
import pellucid.ava.misc.renderers.models.fn_fnc.FnfncModel;
import pellucid.ava.misc.renderers.models.gm94.GM94Model;
import pellucid.ava.misc.renderers.models.m202.M202Model;
import pellucid.ava.misc.renderers.models.m24.M24FleurdelysModel;
import pellucid.ava.misc.renderers.models.m24.M24Model;
import pellucid.ava.misc.renderers.models.m4a1.M4A1DreamcatcherModel;
import pellucid.ava.misc.renderers.models.m4a1.M4A1SumireModel;
import pellucid.ava.misc.renderers.models.m4a1.M4A1XPlorerModel;
import pellucid.ava.misc.renderers.models.m4a1.M4a1Model;
import pellucid.ava.misc.renderers.models.mk18.Mk18AirWarfareModel;
import pellucid.ava.misc.renderers.models.mk18.Mk18KuyomonModel;
import pellucid.ava.misc.renderers.models.mk18.Mk18Model;
import pellucid.ava.misc.renderers.models.mk20.MK20Model;
import pellucid.ava.misc.renderers.models.mosin_nagant.MosinNagantModel;
import pellucid.ava.misc.renderers.models.mp5k.Mp5kFrostModel;
import pellucid.ava.misc.renderers.models.mp5k.Mp5kModel;
import pellucid.ava.misc.renderers.models.mp5sd5.Mp5sd5Model;
import pellucid.ava.misc.renderers.models.p226.P226Model;
import pellucid.ava.misc.renderers.models.python357.Python357Model;
import pellucid.ava.misc.renderers.models.python357.Python357OverRiderModel;
import pellucid.ava.misc.renderers.models.remington870.Reminton870DreamcatcherModel;
import pellucid.ava.misc.renderers.models.remington870.Reminton870Model;
import pellucid.ava.misc.renderers.models.sr_25.Sr25KnutModel;
import pellucid.ava.misc.renderers.models.sr_25.Sr25Model;
import pellucid.ava.misc.renderers.models.sw1911.SW1911ColtModel;
import pellucid.ava.misc.renderers.test.TestModel;
import pellucid.ava.misc.renderers.models.x95r.X95RModel;
import pellucid.ava.misc.renderers.models.xm8.Xm8FrostModel;
import pellucid.ava.misc.renderers.models.xm8.Xm8Model;
import pellucid.ava.misc.renderers.models.xm8.Xm8SnowfallModel;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModelRenderer
{
    @SubscribeEvent
    public static void onModelBake(ModelBakeEvent event)
    {
        for (Item item : Pistols.ITEM_PISTOLS)
            replaceModel(event, item);
        for (Item item : Rifles.ITEM_RIFLES)
            replaceModel(event, item);
        for (Item item : Snipers.ITEM_SNIPERS)
            replaceModel(event, item);
        for (Item item : SubmachineGuns.ITEM_SUBMACHINE_GUNS)
            replaceModel(event, item);
        for (Item item : SpecialWeapons.ITEM_SPECIAL_WEAPONS)
            replaceModel(event, item);
        for (Item item : MeleeWeapons.ITEM_MELEE_WEAPONS)
            replaceModel(event, item);
        ModelResourceLocation loc = new ModelResourceLocation(MiscItems.TEST_ITEM.getRegistryName() + "#inventory");
        event.getModelRegistry().put(loc, new TestModel(event.getModelRegistry().get(loc)));
    }

    private static void replaceModel(ModelBakeEvent event, Item item)
    {
        if (item instanceof ICustomModel)
        {
            ModelResourceLocation loc = new ModelResourceLocation(item.getRegistryName() + "#inventory");
            IBakedModel origin = event.getModelRegistry().get(loc);
            IBakedModel newModel = ((ICustomModel) item).getCustomModel(origin);
            if (origin != null && newModel != null)
                event.getModelRegistry().put(loc, newModel);
        }
    }

    @SubscribeEvent
    public static void onModelRegister(ModelRegistryEvent event)
    {
        P226Model.addSpecialModels();

        MK20Model.addSpecialModels();

        X95RModel.addSpecialModels();

        FnfncModel.addSpecialModels();
        FnfncDreamcatcherModel.addSpecialModels();
        FnfncFullmoonModel.addSpecialModels();

        Xm8Model.addSpecialModels();
        Xm8FrostModel.addSpecialModels();
        Xm8SnowfallModel.addSpecialModels();

        M4a1Model.addSpecialModels();
        M4A1SumireModel.addSpecialModels();
        M4A1DreamcatcherModel.addSpecialModels();
        M4A1XPlorerModel.addSpecialModels();

        MosinNagantModel.addSpecialModels();

        Reminton870Model.addSpecialModels();
        Reminton870DreamcatcherModel.addSpecialModels();

        M24Model.addSpecialModels();
        M24FleurdelysModel.addSpecialModels();

        Mp5sd5Model.addSpecialModels();

        Mk18Model.addSpecialModels();
        Mk18AirWarfareModel.addSpecialModels();
        Mk18KuyomonModel.addSpecialModels();

        Sr25Model.addSpecialModels();
        Sr25KnutModel.addSpecialModels();

        FG42Model.addSpecialModels();
        FG42SumireModel.addSpecialModels();
        FG42DreamcatcherModel.addSpecialModels();

        Mp5kModel.addSpecialModels();
        Mp5kFrostModel.addSpecialModels();

        M202Model.addSpecialModels();
        GM94Model.addSpecialModels();

        SW1911ColtModel.addSpecialModels();

        Python357Model.addSpecialModels();
        Python357OverRiderModel.addSpecialModels();
    }

    @SubscribeEvent
    public static void onTextureStitch(TextureStitchEvent.Pre event)
    {
        ModifiedGunModel.addTextureAtlasSprite(event);
    }
}
