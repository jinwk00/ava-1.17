package pellucid.ava.misc.renderers;

import net.minecraft.client.renderer.Vector3f;

import javax.annotation.Nullable;

public class Perspective
{
    public final Vector3f rotation;
    public final Vector3f translation;
    public final Vector3f scale;

    public Perspective()
    {
        this(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
    }

    public Perspective(float rotationX, float rotationY, float rotationZ, float translationX, float translationY, float translationZ, float scaleX, float scaleY, float scaleZ)
    {
        this(new Vector3f(rotationX, rotationY, rotationZ), new Vector3f(translationX, translationY, translationZ), new Vector3f(scaleX, scaleY, scaleZ));
    }

    public Perspective(float[] rotation, float[] translation, float[] scale)
    {
        this(new Vector3f(rotation), new Vector3f(translation), new Vector3f(scale));
    }

    public Perspective(Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        this.rotation = rotation;
        this.translation = translation;
        this.scale = scale;
    }

    public static Perspective copy(@Nullable Perspective perspective)
    {
        return perspective == null ? null : new Perspective(perspective.rotation.copy(), perspective.translation.copy(), perspective.scale.copy());
    }

    public float[] getRotation()
    {
        return new float[]{this.rotation.getX(), this.rotation.getY(), this.rotation.getZ()};
    }

    public float[] getTranslation()
    {
        return new float[]{this.translation.getX(), this.translation.getY(), this.translation.getZ()};
    }

    public float[] getScale()
    {
        return new float[]{this.scale.getX(), this.scale.getY(), this.scale.getZ()};
    }
}
