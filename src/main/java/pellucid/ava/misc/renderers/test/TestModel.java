package pellucid.ava.misc.renderers.test;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.client.model.data.EmptyModelData;
import net.minecraftforge.common.model.TransformationHelper;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class TestModel implements IBakedModel
{
    protected IBakedModel origin;

    public TestModel(IBakedModel origin)
    {
        this.origin = origin;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        return this.origin.getQuads(state, side, rand);
    }

    @Override
    public boolean isAmbientOcclusion()
    {
        return this.origin.isAmbientOcclusion();
    }

    @Override
    public boolean isGui3d()
    {
        return this.origin.isGui3d();
    }

    @Override
    public boolean func_230044_c_()
    {
        return this.origin.func_230044_c_();
    }

    @Override
    public boolean isBuiltInRenderer()
    {
        return this.origin.isBuiltInRenderer();
    }

    @Override
    public TextureAtlasSprite getParticleTexture()
    {
        return this.origin.getParticleTexture(EmptyModelData.INSTANCE);
    }

    @Override
    public ItemOverrideList getOverrides()
    {
        return new Overrides();
    }

    public static class Overrides extends ItemOverrideList
    {
        public Overrides()
        {
            super();
        }

        @Nullable
        @Override
        public IBakedModel getModelWithOverrides(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
        {
            return new ModifiedTestModel(origin, world);
        }
    }

    static class ModifiedTestModel implements IBakedModel
    {
        protected IBakedModel origin;
        protected World world;

        public ModifiedTestModel(IBakedModel origin, World world)
        {
            this.origin = origin;
            this.world = world;
        }

        @Override
        public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
        {
//            origin.getQuads(state, side, rand).forEach((quad) -> {
//                float[] arr = new float[quad.getVertexData().length];
//                for (int i = 0; i < quad.getVertexData().length; i++)
//                    arr[i] = Float.intBitsToFloat(quad.getVertexData()[i]) * 16;
//                System.out.println(Arrays.toString(arr));
//            });
//            List<BakedQuad> quads = new ArrayList<>();
            List<BakedQuad> quads = new ArrayList<>(origin.getQuads(state, side, rand));
//            for (BakedQuad quad : origin.getQuads(state, side, rand))
//            {
//                BakedQuad newQuad = new BakedQuad(quad.getVertexData().clone(), quad.getTintIndex(), quad.getFace(), quad.getSprite(), quad.applyDiffuseLighting());
//                rotateQuad(newQuad, Direction.Axis.Z, 45);
//                quads.add(newQuad);
//            }
            if (quads.isEmpty() || world == null)
                return quads;
            for (BakedQuad quad : quads)
                rotateQuad(quad, Direction.Axis.Z, 0.05F, null);
//            BakedQuadBuilder builder = new BakedQuadBuilder(quads.get(0).getSprite());
//            for (int i=0;i<4;i++)
//            {
//                ImmutableList<VertexFormatElement> elements = builder.getVertexFormat().getElements().asList();
//                for (int j = 0; j < elements.size(); j++)
//                {
//                    VertexFormatElement e = elements.get(j);
//                    switch (e.getUsage())
//                    {
//                        case POSITION:
//                            builder.put(j, (float) 4, (float) 4, (float) 4, 1.0f);
//                            break;
//                        case COLOR:
//                            builder.put(j, 0, 0, 0, 1.0f);
//                            break;
//                        case UV:
//                            builder.put(j, 0, 0);
//                            break;
//                        case NORMAL:
//                            builder.put(j, (float) 2, (float) 2, (float) 2);
//                            break;
//                        default:
//                            builder.put(j);
//                            break;
//                    }
//                }
//            }
//            BakedQuad q = builder.build();
//            float[] arr = new float[q.getVertexData().length];
//            for (int i = 0; i < q.getVertexData().length; i++)
//                arr[i] = i % 8 < 3 ? Float.intBitsToFloat(q.getVertexData()[i]) : q.getVertexData()[i];
//            System.out.println(Arrays.toString(arr));
//            Vector3f offsets = new Vector3f(8.0F, 15.0F, 8.0F);
//            offsets.mul(-0.0625F, -0.0625F, 0);
//            QuadTransformer transformer = new QuadTransformer(new TransformationMatrix(offsets, null, null, null));
//            QuadTransformer transformer2 = new QuadTransformer(new TransformationMatrix(null, TransformationHelper.quatFromXYZ(new Vector3f(0.0F, 0.0F, 45.0F), true), null, null));
//            offsets.mul(-1.0F, -1.0F, 0.0F);
//            QuadTransformer transformer3 = new QuadTransformer(new TransformationMatrix(offsets, null, null, null));
//            return transformer3.processMany(transformer2.processMany(transformer.processMany(quads)));
//            return transformer2.processMany(transformer.processMany(quads));
            return quads;
//            List<BakedQuad> newQUads = new ArrayList<>();
//            for (BakedQuad quad : quads)
//            {
//                BakedQuad newQuad = new BakedQuad(quad.getVertexData().clone(), quad.getTintIndex(), quad.getFace(), quad.getSprite(), quad.applyDiffuseLighting());
//                rotateQuad(newQuad, Direction.Axis.Z, 90, null);
//                newQUads.add(newQuad);
//            }
//            return newQUads;
        }

        protected void rotateQuad(BakedQuad quad, Direction.Axis direction, float amount, Vector3f pivot)
        {
            int index;
            int[] vertex = quad.getVertexData();
            Vector3f offsets = new Vector3f(8.0F, 15.0F, 8.0F);
            offsets.mul(0.0625F, 0.0625F, 0);
            for (index=0;index<vertex.length; index+=8)
            {
                float x2 = Float.intBitsToFloat(vertex[index]);
                float y2 = Float.intBitsToFloat(vertex[index + 1]);
                float z2 = Float.intBitsToFloat(vertex[index + 2]);
                float x = x2 - offsets.getX();
                float y = y2 - offsets.getY();
                float z = z2 - offsets.getZ();
                if (direction == Direction.Axis.X)
                {
                    vertex[index + 1] = Float.floatToRawIntBits((float) (y * cos(amount) - z * sin(amount)) + offsets.getY());
                    vertex[index + 2] = Float.floatToRawIntBits((float) (z * cos(amount) + y * sin(amount)) + offsets.getZ());
                }
                else if (direction == Direction.Axis.Y)
                {
                    vertex[index] = Float.floatToRawIntBits((float) (x * cos(amount) + z * sin(amount)) + offsets.getX());
                    vertex[index + 2] = Float.floatToRawIntBits((float) (z * cos(amount) - x * sin(amount)) + offsets.getZ());
                }
                else
                {
                    vertex[index] = Float.floatToRawIntBits((float) (x * cos(amount) - y * sin(amount)) + offsets.getX());
                    vertex[index + 1] = Float.floatToRawIntBits((float) (y * cos(amount) + x * sin(amount)) + offsets.getY());
                }
            }
        }

        @Override
        public IBakedModel handlePerspective(ItemCameraTransforms.TransformType cameraTransformType, MatrixStack mat)
        {
            Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
            Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
            Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
            TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
            if (!transformationMatrix.isIdentity())
                transformationMatrix.push(mat);
            return getBakedModel();
        }

        protected TransformationMatrix getTransformationMatrix(@Nullable Vector3f rotation, @Nullable Vector3f translation, @Nullable Vector3f scale)
        {
            if (translation != null)
            {
                translation.mul(0.0625F);
                translation.clamp(-5.0F, 5.0F);
            }
            if (scale != null)
                scale.clamp(-4.0F, 4.0F);
            return new TransformationMatrix(translation, TransformationHelper.quatFromXYZ(rotation, true), scale, null);
        }

        @Override
        public boolean isAmbientOcclusion()
        {
            return this.origin.isAmbientOcclusion();
        }

        @Override
        public boolean isGui3d()
        {
            return this.origin.isGui3d();
        }

        @Override
        public boolean func_230044_c_()
        {
            return this.origin.func_230044_c_();
        }

        @Override
        public boolean isBuiltInRenderer()
        {
            return this.origin.isBuiltInRenderer();
        }

        public TextureAtlasSprite getParticleTexture()
        {
            return origin.getParticleTexture();
        }

        @Override
        public ItemOverrideList getOverrides()
        {
            return null;
        }
    }
}
