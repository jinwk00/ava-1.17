package pellucid.ava.misc.renderers;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.TransformationMatrix;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.client.model.QuadTransformer;
import net.minecraftforge.common.model.TransformationHelper;
import pellucid.ava.events.AVACommonEvent;
import pellucid.ava.misc.config.AVAClientConfig;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import static java.lang.Math.abs;

public abstract class BakedModel implements IBakedModel
{
    public static final float[] ONE_A = fArr(1);
    public static final Vector3f ONE_V = v3f(1);

    protected IBakedModel origin;
    public static final Random RAND = new Random();
    protected ItemStack stack;
    protected World world;
    protected LivingEntity entity;

    public BakedModel(IBakedModel origin, ItemStack stack, @Nullable World world, @Nullable LivingEntity entity)
    {
        this.origin = origin;
        this.stack = stack;
        this.world = world;
        this.entity = entity;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        return origin.getQuads(state, side, rand);
    }

    @Override
    public boolean isAmbientOcclusion()
    {
        return origin.isAmbientOcclusion();
    }

    @Override
    public boolean isGui3d()
    {
        return origin.isGui3d();
    }

    @Override
    public boolean func_230044_c_()
    {
        return false;
    }

    @Override
    public boolean isBuiltInRenderer()
    {
        return origin.isBuiltInRenderer();
    }

    @Override
    public TextureAtlasSprite getParticleTexture()
    {
        return origin.getParticleTexture();
    }

    @Override
    public ItemOverrideList getOverrides()
    {
        return null;
    }

    protected boolean translateQuadFrom(BakedQuad quad, float awayFrom, int currentTicks, int fromTicks, int toTicks, Direction... directions)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        awayFrom = abs(awayFrom);
        translateQuad(quad, -awayFrom + awayFrom / (toTicks - fromTicks) * (currentTicks - fromTicks), directions);
        return true;
    }

    protected boolean translateQuadTo(BakedQuad quad, float amount, int currentTicks, int fromTicks, int toTicks, Direction... directions)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        translateQuad(quad, amount / (toTicks - fromTicks) * (currentTicks - fromTicks), directions);
        return true;
    }

    protected boolean translateQuad(BakedQuad quad, float amount, int currentTicks, int fromTicks, int toTicks, Direction... directions)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        translateQuad(quad, amount, directions);
        return true;
    }

    protected void translateQuad(BakedQuad quad, float amount, Direction... directions)
    {
        for (Direction direction : directions)
            translateQuad(quad, direction, amount);
    }

    protected void translateQuad(BakedQuad quad, Direction direction, float amount)
    {
        int index;
        int[] vertex = quad.getVertexData();
        amount /= 16.0F;
        switch (direction)
        {
            case NORTH:
            case SOUTH:
            default:
                index = 2;
                if (direction == Direction.NORTH)
                    amount = -amount;
                break;
            case UP:
            case DOWN:
                index = 1;
                if (direction == Direction.DOWN)
                    amount = -amount;
                break;
            case EAST:
            case WEST:
                index = 0;
                if (direction == Direction.WEST)
                    amount = -amount;
                break;
        }
        for (;index<vertex.length; index+=8)
            vertex[index] = Float.floatToRawIntBits(Float.intBitsToFloat(vertex[index]) + amount);
    }

    protected List<BakedQuad> rotateQuad(List<BakedQuad> quads, float amount, int currentTicks, int fromTicks, int toTicks, Direction.Axis direction, Vector3f pivot)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return quads;
        return rotateQuad(quads, direction, amount / (toTicks - fromTicks) * (currentTicks - fromTicks), pivot);
    }

    protected List<BakedQuad> rotateQuad(List<BakedQuad> quads, Direction.Axis direction, float amount, Vector3f offsets)
    {
        Vector3f rotation = new Vector3f();
        switch (direction)
        {
            case X:
            default:
                rotation.setX(1.0F);
                break;
            case Y:
                rotation.setY(1.0F);
                break;
            case Z:
                rotation.setZ(1.0F);
                break;
        }
        rotation.mul(amount);
        offsets.mul(-0.0625F, -0.0625F, 0);
        QuadTransformer transformer = new QuadTransformer(new TransformationMatrix(offsets, null, null, null));
        QuadTransformer transformer2 = new QuadTransformer(new TransformationMatrix(null, TransformationHelper.quatFromXYZ(rotation, true), null, null));
        offsets.mul(-1.0F, -1.0F, 0.0F);
        QuadTransformer transformer3 = new QuadTransformer(new TransformationMatrix(offsets, null, null, null));
        return transformer3.processMany(transformer2.processMany(transformer.processMany(quads)));
        //        List<BakedQuad> quads1 = new ArrayList<>();
        //        offsets.mul(0.0625F, 0.0625F, 0.0625F);
        //        for (BakedQuad quad : quads)
        //        {
        //            int index;
        //            int[] vertex = quad.getVertexData();
        //            for (index=0;index<vertex.length; index+=8)
        //            {
        //                float x2 = Float.intBitsToFloat(vertex[index]);
        //                float y2 = Float.intBitsToFloat(vertex[index + 1]);
        //                float z2 = Float.intBitsToFloat(vertex[index + 2]);
        //                float x = x2 - offsets.getX();
        //                float y = y2 - offsets.getY();
        //                float z = z2 - offsets.getZ();
        //                if (direction == Direction.Axis.X)
        //                {
        //                    vertex[index + 1] = Float.floatToRawIntBits((float) (y * cos(amount) - z * sin(amount)) + offsets.getY());
        //                    vertex[index + 2] = Float.floatToRawIntBits((float) (z * cos(amount) + y * sin(amount)) + offsets.getZ());
        //                }
        //                else if (direction == Direction.Axis.Y)
        //                {
        //                    vertex[index] = Float.floatToRawIntBits((float) (x * cos(amount) + z * sin(amount)) + offsets.getX());
        //                    vertex[index + 2] = Float.floatToRawIntBits((float) (z * cos(amount) - x * sin(amount)) + offsets.getZ());
        //                }
        //                else
        //                {
        //                    vertex[index] = Float.floatToRawIntBits((float) (x * cos(amount) - y * sin(amount)) + offsets.getX());
        //                    vertex[index + 1] = Float.floatToRawIntBits((float) (y * cos(amount) + x * sin(amount)) + offsets.getY());
        //                }
        //            }
        //            quads1.add(quad);
        //        }
        //        return quads1;
    }

    protected void merge(List<BakedQuad> origin, ModelResourceLocation rel, BlockState state, Random random)
    {
        this.merge(origin, rel, state, random, (quad) -> {});
    }

    protected void merge(List<BakedQuad> origin, ModelResourceLocation rel, BlockState state, Random random, Consumer<BakedQuad> modify)
    {
        origin.addAll(this.get(rel, state, random, modify));
    }

    protected List<BakedQuad> get(ModelResourceLocation rel)
    {
        return get(rel, null, null, (quad) -> {});
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, BlockState state, Random random)
    {
        return get(rel, state, random, (quad) -> {});
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, Consumer<BakedQuad> modify)
    {
        return get(rel, null, null, modify);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, BlockState state, Random random, Consumer<BakedQuad> modify)
    {
        return get(rel, state, random, modify, false);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, Consumer<BakedQuad> modify, boolean empty)
    {
        return get(rel, null, null, modify, false);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, BlockState state, Random random, Consumer<BakedQuad> modify, boolean empty)
    {
        if (empty)
            return Collections.emptyList();
        IBakedModel newModel = Minecraft.getInstance().getModelManager().getModel(rel);
        ArrayList<BakedQuad> origin = new ArrayList<>();
        for (BakedQuad quad : newModel.getQuads(state, null, random))
        {
            BakedQuad newQuad = copyQuad(quad);
            modify.accept(newQuad);
            origin.add(newQuad);
        }
        return origin;
    }

    protected BakedQuad copyQuad(BakedQuad quad)
    {
        return new BakedQuad(quad.getVertexData().clone(), quad.getTintIndex(), quad.getFace(), quad.func_187508_a(), quad.shouldApplyDiffuseLighting());
    }

    public static Perspective getPerspectiveInBetween(ArrayList<Animation> animations, int currentTicks)
    {
        Perspective perspective = new Perspective();
        Animation prevAnimation = animations.get(0);
        for (Animation animation : animations)
        {
            if (currentTicks > animation.targetTicks)
                prevAnimation = animation;
            else
            {
                perspective = prevAnimation.getPerspectiveInBetween(animation, currentTicks);
                break;
            }
        }
        return perspective;
    }


    public static void copy(Perspective from, Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        rotation.set(from.getRotation());
        translation.set(from.getTranslation());
        scale.set(from.getScale());
    }

    public static void bob(Vector3f rotation, Vector3f translation, int tick)
    {
        if (!AVAClientConfig.FP_BOBBING.get())
            return;
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null)
        {
            int e = tick % 120;
            if (e < 30)
            {
                e++;
                rotation.set(rotation.getX() - 2.0F / 30.0F * e, rotation.getY(), rotation.getZ());
                translation.set(translation.getX(), translation.getY() + 0.35F / 30.0F * e, translation.getZ());
            }
            else if (e < 60)
            {
                e -= 29;
                rotation.set(rotation.getX() - 2.0F + 2.0F / 30.0F * e, rotation.getY(), rotation.getZ());
                translation.set(translation.getX(), translation.getY() + 0.35F + 0.35F / 30.0F * e, translation.getZ());
            }
            else if (e < 90)
            {
                e -= 59;
                rotation.set(rotation.getX() + 2.0F / 30.0F * e, rotation.getY(), rotation.getZ());
                translation.set(translation.getX(), translation.getY() + 0.7F - 0.35F / 30.0F * e, translation.getZ());
            }
            else
            {
                e -= 89;
                rotation.set(rotation.getX() + 2.0F - 2.0F / 30.0F * e, rotation.getY(), rotation.getZ());
                translation.set(translation.getX(), translation.getY() + 0.35F - 0.35F / 30.0F * e, translation.getZ());
            }
            if (AVACommonEvent.isMovingOnGroundClient(player))
            {
                if (player.isSneaking())
                {
                    e = player.ticksExisted % 60;
                    if (e < 15)
                    {
                        e++;
                        translation.set(translation.getX() + 0.5F / 15.0F * e, translation.getY(), translation.getZ());
                    }
                    else if (e < 30)
                    {
                        e -= 14;
                        translation.set(translation.getX() + 0.5F - 0.5F / 15.0F * e, translation.getY(), translation.getZ());
                    }
                    else if (e < 45)
                    {
                        e -= 29;
                        translation.set(translation.getX() - 0.5F / 15.0F * e, translation.getY(), translation.getZ());
                    }
                    else
                    {
                        e -= 44;
                        translation.set(translation.getX() - 0.5F + 0.5F / 15.0F * e, translation.getY(), translation.getZ());
                    }
                }
                else
                {
                    e = player.ticksExisted % 8;
                    if (e < 2)
                    {
                        e++;
                        translation.set(translation.getX(), translation.getY() + 0.4F / 4.0F * e, translation.getZ());
                        translation.set(translation.getX(), translation.getY(), translation.getZ() + 0.25F / 4.0F * e);
                    }
                    else if (e < 4)
                    {
                        e -= 1;
                        translation.set(translation.getX(), translation.getY() + 0.4F - 0.4F / 4.0F * e, translation.getZ());
                        translation.set(translation.getX(), translation.getY(), translation.getZ() + 0.25F - 0.25F / 4.0F * e);
                    }
                    else if (e < 6)
                    {
                        e -= 3;
                        translation.set(translation.getX(), translation.getY() - 0.4F / 4.0F * e, translation.getZ());
                        translation.set(translation.getX(), translation.getY(), translation.getZ() - 0.25F / 4.0F * e);
                    }
                    else
                    {
                        e -= 5;
                        translation.set(translation.getX(), translation.getY() - 0.4F + 0.4F / 4.0F * e, translation.getZ());
                        translation.set(translation.getX(), translation.getY(), translation.getZ() - 0.25F + 0.25F / 4.0F * e);
                    }
                    e = player.ticksExisted % 16;
                    if (e < 4)
                    {
                        e++;
                        translation.set(translation.getX() + 0.3F / 4.0F * e, translation.getY(), translation.getZ());
                    }
                    else if (e < 12)
                    {
                        e -= 3;
                        translation.set(translation.getX() + 0.3F - 0.6F / 8.0F * e, translation.getY(), translation.getZ());
                    }
                    else
                    {
                        e -= 11;
                        translation.set(translation.getX() -0.3F + 0.3F / 4.0F * e, translation.getY(), translation.getZ());
                    }
                }
            }
        }
    }

    protected TransformationMatrix getTransformationMatrix(@Nullable Vector3f rotation, @Nullable Vector3f translation, @Nullable Vector3f scale)
    {
        if (translation != null)
        {
            translation.mul(0.0625F);
            translation.clamp(-5.0F, 5.0F);
        }
        if (scale != null)
            scale.clamp(-4.0F, 4.0F);
        return new TransformationMatrix(translation, TransformationHelper.quatFromXYZ(rotation, true), scale, null);
    }

    public static Vector3f v3f(float f)
    {
        return new Vector3f(f, f, f);
    }

    public static float[] fArr(float f)
    {
        return new float[]{f, f, f};
    }
}
