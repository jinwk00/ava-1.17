package pellucid.ava.misc.renderers;

import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public interface ICustomBakedModelGetter
{
    IBakedModel getBakedModel(IBakedModel originalBakedModel);
}
