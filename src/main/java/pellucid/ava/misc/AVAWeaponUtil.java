package pellucid.ava.misc;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mojang.datafixers.util.Pair;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.ProtectionEnchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.EndermanEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.*;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.PacketDistributor;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.entities.AVAEntity;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.items.init.*;
import pellucid.ava.misc.cap.IWorldData;
import pellucid.ava.misc.cap.PlayerAction;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.packets.AVAPackets;
import pellucid.ava.misc.packets.FlashMessage;
import pellucid.ava.misc.packets.PlaySoundToClientMessage;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.BiConsumer;

import static pellucid.ava.misc.config.AVAServerConfig.*;

public class AVAWeaponUtil
{
    public static void createExplosion(AVAEntity projectile, @Nullable LivingEntity thrower, Item weapon, BiConsumer<Entity, Double> extraAction, TriConsumer<ServerWorld, Double, Double> effect, SoundEvent explosionSound)
    {
        World world = projectile.world;
        if (!world.isRemote())
        {
            double radius = getExplosiveRadiusForEntity(projectile);
            double damage = getExplosiveDamageForEntity(projectile);
            boolean destroysEntities = EXPLOSION_DESTROYS_ENTITIES.get();
            AxisAlignedBB bb = projectile.getBoundingBox();
            if (radius <= 0)
                return;
            Vec3d position = projectile.getPositionVec();
            for (Entity entity : world.getEntitiesInAABBexcluding(projectile, bb.grow(radius), entity -> !projectile.fromMob || entity instanceof PlayerEntity))
            {
                int i;
                if (!entity.isAlive() || (!(entity instanceof LivingEntity) && !destroysEntities))
                    continue;
                double distance = Double.MAX_VALUE;
                for (i=0;i<3;i++)
                {
                    Vec3d hitPos = canBeSeen(projectile, entity);
                    if (hitPos != null)
                    {
                        double distance2 = hitPos.distanceTo(position);
                        if (distance2 < distance)
                            distance = distance2;
                    }
                }
                if (distance == Double.MAX_VALUE)
                    continue;
                damage = damage * Math.max((radius - distance) / radius, 0.35F);
                if (damage > 0)
                    if (!attackEntityDependAllyDamage(entity, AVADamageSource.causeProjectileExplodeDamage(thrower, projectile, weapon), (float) damage))
                        continue;
                extraAction.accept(entity, distance);
            }
            for (double x = position.getX() - radius; x < position.getX() + radius; x++)
                for (double z = position.getZ() - radius; z < position.getZ() + radius; z++)
                    effect.accept((ServerWorld) world, x, z);
            playAttenuableSoundToClient(explosionSound, position.getX(), position.getY(), position.getZ());
            createExplosionForBlocks(world, projectile, projectile.getPosX(), projectile.getPosY(), projectile.getPosZ(), radius);
        }
    }

    public static void createFlash(AVAEntity projectile, TriConsumer<ServerWorld, Double, Double> effect, SoundEvent explosionSound)
    {
        World world = projectile.world;
        if (!world.isRemote())
        {
            double radius = getFlashRadiusForEntity(projectile);
            AxisAlignedBB bb = projectile.getBoundingBox();
            int flash = getFlashDurationForEntity(projectile);
            if (radius <= 0 || flash <= 0)
                return;
            Vec3d position = projectile.getPositionVec();
            for (Entity entity : world.getEntitiesInAABBexcluding(projectile, bb.grow(radius), entity -> entity instanceof PlayerEntity))
            {
                int i;
                if (!entity.isAlive())
                    continue;
                double distance = Double.MAX_VALUE;
                for (i=0;i<3;i++)
                {
                    Vec3d hitPos = canBeSeen(projectile, entity);
                    if (hitPos != null)
                    {
                        double distance2 = hitPos.distanceTo(position);
                        if (distance2 < distance)
                            distance = distance2;
                    }
                }
                if (distance == Double.MAX_VALUE)
                    continue;
                flash = (int) (flash * Math.max((radius - distance) / radius, 0.15F));
                if (!((PlayerEntity) entity).abilities.isCreativeMode && flash > 0)
                    AVAPackets.INSTANCE.sendTo(new FlashMessage(flash), ((ServerPlayerEntity) entity).connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
            }
            for (double x = position.getX() - radius; x < position.getX() + radius; x++)
                for (double z = position.getZ() - radius; z < position.getZ() + radius; z++)
                    effect.accept((ServerWorld) world, x, z);
            playAttenuableSoundToClient(explosionSound, position.getX(), position.getY(), position.getZ());
        }
    }

    public static void playAttenuableSoundToClientMoving(SoundEvent sound, Entity moving)
    {
        if (moving.world instanceof ServerWorld)
            AVAPackets.INSTANCE.send(PacketDistributor.ALL.noArg(), new PlaySoundToClientMessage(sound, -1, -1, -1).setMoving(moving.getEntityId()));
    }

    public static void playAttenuableSoundToClient(SoundEvent sound, double x, double y, double z)
    {
        AVAPackets.INSTANCE.send(PacketDistributor.ALL.noArg(), new PlaySoundToClientMessage(sound, x, y, z));
    }

    public static void createExplosionForBlocks(World world, Entity projectile, double x, double y, double z, double size)
    {
        if (!world.isRemote())
            new ExplosionForBlocks(world, projectile, x, y, z, size);
    }

    public static boolean destroyGlassOnHit(World world, BlockPos pos)
    {
        if (world.getBlockState(pos).getMaterial() == Material.GLASS && WorldData.getCap(world).isGlassDestroyable())
        {
            world.destroyBlock(pos, false);
            return true;
        }
        return false;
    }

    public static boolean attackEntityDependAllyDamage(Entity target, EntityDamageSource source, float damage)
    {
        IWorldData cap = WorldData.getCap(target.world);
        Entity attacker = source.getTrueSource();
        boolean friendlyFire = (attacker != null && target.isOnSameTeam(attacker)) && attacker != target;
        if (!cap.isFriendlyFireAllowed() && friendlyFire)
            return false;
        float multiplier = friendlyFire && cap.isFriendlyFireReduced() ? 0.25F : 1.0F;
        return attackEntity(target, source, damage, multiplier);
    }

    public static boolean attackEntity(Entity target, EntityDamageSource source, float damage, float multiplier)
    {
        Entity attacker = source.getTrueSource();
        if (attacker instanceof PlayerEntity)
            damage += PlayerAction.getCap((PlayerEntity) attacker).getAttackDamageBoost();
        else if (attacker instanceof AVAHostileEntity)
        {
            IAttributeInstance attribute = ((AVAHostileEntity) attacker).getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
            damage += attribute.getValue();
        }
        if (target instanceof EndermanEntity && source instanceof AVADamageSource.IWeapon)
            source = AVADamageSource.causeDamageDirect(source.getTrueSource(), ((AVADamageSource.IWeapon) source).getWeapon());
        damage *= multiplier;
        damage *= target instanceof PlayerEntity ? DAMAGE_MULTIPLIER_AGAINST_PLAYERS.get() : DAMAGE_MULTIPLIER_AGAINST_OTHERS.get();
        return target.attackEntityFrom(source, damage);
    }

    public static Vec3d canBeSeen(Entity from, Entity to)
    {
        return canBeSeen(from, to, from.world, from.getPositionVec(), to.getPositionVec());
    }

    @Nullable
    public static Vec3d canBeSeen(@Nullable Entity attacker, Entity target, World world, Vec3d from, Vec3d to)
    {
        for (int i=0;i<3;i++)
        {
            Vec3d to2 = to.add(0.0F, target.getHeight() / 2 * i, 0.0F);
            if (canBeSeen(world, from, to2, attacker))
                return to2;
        }
        return null;
    }

    public static boolean canBeSeen(World world, Vec3d from, Vec3d to, @Nullable Entity attacker)
    {
        return world.rayTraceBlocks(new RayTraceContext(from, to, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, attacker)).getType() != BlockRayTraceResult.Type.BLOCK;
    }

    public static double getExplosiveDamageForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.world.isRemote())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0.0D;
            if (weapon == Projectiles.M67)
                return M67_EXPLOSIVE_DAMAGE.get();
            else if (weapon == Projectiles.MK3A2)
                return MK3A2_EXPLOSIVE_DAMAGE.get();
            else if (weapon == SpecialWeapons.M202)
                return M202_ROCKET_EXPLOSIVE_DAMAGE.get();
            else if (weapon == SpecialWeapons.GM94)
                return GM94_GRENADE_EXPLOSIVE_DAMAGE.get();
        }
        return 0.0D;
    }

    public static double getExplosiveRadiusForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.world.isRemote())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0.0D;
            if (weapon == Projectiles.M67)
                return M67_EXPLOSIVE_RANGE.get();
            else if (weapon == Projectiles.MK3A2)
                return MK3A2_EXPLOSIVE_RANGE.get();
            else if (weapon == SpecialWeapons.M202)
                return M202_ROCKET_EXPLOSIVE_RANGE.get();
            else if (weapon == SpecialWeapons.GM94)
                return GM94_GRENADE_EXPLOSIVE_RANGE.get();
        }
        return 0.0D;
    }

    public static double getFlashRadiusForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.world.isRemote())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0.0D;
            if (weapon == Projectiles.M116A1)
                return M116A1_FLASH_RANGE.get();
            else if (weapon == Projectiles.MK3A2)
                return MK3A2_FLASH_RANGE.get();
        }
        return 0.0D;
    }

    public static int getFlashDurationForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.world.isRemote())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0;
            if (weapon == Projectiles.M116A1)
                return M116A1_FLASH_DURATION.get();
            else if (weapon == Projectiles.MK3A2)
                return MK3A2_FLASH_DURATION.get();
        }
        return 0;
    }

    public static class ExplosionForBlocks extends Explosion
    {
        private final boolean causesFire = false;
        private final Explosion.Mode mode = Mode.DESTROY;
        private final Random random = new Random();
        private final World world;
        private final double x;
        private final double y;
        private final double z;
        @Nullable
        private final Entity exploder;
        private final float size;
        private DamageSource damageSource;
        private final List<BlockPos> affectedBlockPositions = Lists.newArrayList();
        private final Map<PlayerEntity, Vec3d> playerKnockbackMap = Maps.newHashMap();
        private final Vec3d position;

        public ExplosionForBlocks(World world, @Nullable Entity exploder, double x, double y, double z, double size)
        {
            super(world, exploder, x, y, z, (float) size, false, Mode.DESTROY);
            this.world = world;
            this.exploder = exploder;
            this.size = (float) size;
            this.x = x;
            this.y = y;
            this.z = z;
            this.damageSource = DamageSource.causeExplosionDamage(this);
            this.position = new Vec3d(this.x, this.y, this.z);
            if (!net.minecraftforge.event.ForgeEventFactory.onExplosionStart(world, this))
            {
                doExplosionA();
                doExplosionB();
            }
        }

        public void doExplosionA()
        {
            int destructionType = EXPLOSION_DESTRUCTION_TYPE.get();
            if (destructionType == 0)
                return;
            Set<BlockPos> set = Sets.newHashSet();
            for(int j = 0; j < 16; ++j)
                for(int k = 0; k < 16; ++k)
                    for(int l = 0; l < 16; ++l)
                        if (j == 0 || j == 15 || k == 0 || k == 15 || l == 0 || l == 15)
                        {
                            double d0 = (float)j / 15.0F * 2.0F - 1.0F;
                            double d1 = (float)k / 15.0F * 2.0F - 1.0F;
                            double d2 = (float)l / 15.0F * 2.0F - 1.0F;
                            double d3 = Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
                            d0 = d0 / d3;
                            d1 = d1 / d3;
                            d2 = d2 / d3;
                            float f = this.size * (0.7F + this.world.rand.nextFloat() * 0.6F);
                            double d4 = this.x;
                            double d6 = this.y;
                            double d8 = this.z;
                            for(; f > 0.0F; f -= 0.22500001F)
                            {
                                BlockPos blockpos = new BlockPos(d4, d6, d8);
                                BlockState blockstate = this.world.getBlockState(blockpos);
                                IFluidState ifluidstate = this.world.getFluidState(blockpos);
                                if (!blockstate.isAir(this.world, blockpos) || !ifluidstate.isEmpty())
                                {
                                    float f2 = Math.max(blockstate.getExplosionResistance(this.world, blockpos, exploder, this), ifluidstate.getExplosionResistance(this.world, blockpos, exploder, this));
                                    if (this.exploder != null)
                                        f2 = this.exploder.getExplosionResistance(this, this.world, blockpos, blockstate, ifluidstate, f2);
                                    f -= (f2 + 0.3F) * 0.3F;
                                }
                                if (f > 0.0F && (this.exploder == null || this.exploder.canExplosionDestroyBlock(this, this.world, blockpos, blockstate, f)))
                                {
                                    Material material = blockstate.getMaterial();
                                    if (destructionType == 1)
                                    {
                                        if (material == Material.GLASS)
                                            set.add(blockpos);
                                    }
                                    else
                                        set.add(blockpos);
                                }
                                d4 += d0 * (double)0.3F;
                                d6 += d1 * (double)0.3F;
                                d8 += d2 * (double)0.3F;
                            }
                        }
            this.affectedBlockPositions.addAll(set);
            float f3 = this.size * 2.0F;
            int k1 = MathHelper.floor(this.x - (double)f3 - 1.0D);
            int l1 = MathHelper.floor(this.x + (double)f3 + 1.0D);
            int i2 = MathHelper.floor(this.y - (double)f3 - 1.0D);
            int i1 = MathHelper.floor(this.y + (double)f3 + 1.0D);
            int j2 = MathHelper.floor(this.z - (double)f3 - 1.0D);
            int j1 = MathHelper.floor(this.z + (double)f3 + 1.0D);
            List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this.exploder, new AxisAlignedBB(k1, i2, j2, l1, i1, j1));
            net.minecraftforge.event.ForgeEventFactory.onExplosionDetonate(this.world, this, list, f3);
            Vec3d vec3d = new Vec3d(this.x, this.y, this.z);
            for(int k2 = 0; k2 < list.size(); ++k2)
            {
                Entity entity = list.get(k2);
                if (!entity.isImmuneToExplosions())
                {
                    double d12 = MathHelper.sqrt(entity.getDistanceSq(vec3d)) / f3;
                    if (d12 <= 1.0D)
                    {
                        double d5 = entity.getPosX() - this.x;
                        double d7 = entity.getPosYEye() - this.y;
                        double d9 = entity.getPosZ() - this.z;
                        double d13 = MathHelper.sqrt(d5 * d5 + d7 * d7 + d9 * d9);
                        if (d13 != 0.0D)
                        {
                            d5 = d5 / d13;
                            d7 = d7 / d13;
                            d9 = d9 / d13;
                            double d14 = getBlockDensity(vec3d, entity);
                            double d10 = (1.0D - d12) * d14;
                            entity.attackEntityFrom(this.getDamageSource(), (float)((int)((d10 * d10 + d10) / 2.0D * 7.0D * (double)f3 + 1.0D)));
                            double d11 = d10;
                            if (entity instanceof LivingEntity)
                                d11 = ProtectionEnchantment.getBlastDamageReduction((LivingEntity)entity, d10);
                            entity.setMotion(entity.getMotion().add(d5 * d11, d7 * d11, d9 * d11));
                            if (entity instanceof PlayerEntity)
                            {
                                PlayerEntity playerentity = (PlayerEntity)entity;
                                if (!playerentity.isSpectator() && (!playerentity.isCreative() || !playerentity.abilities.isFlying))
                                    this.playerKnockbackMap.put(playerentity, new Vec3d(d5 * d10, d7 * d10, d9 * d10));
                            }
                        }
                    }
                }
            }
        }

        public void doExplosionB()
        {
            ObjectArrayList<Pair<ItemStack, BlockPos>> objectarraylist = new ObjectArrayList<>();
            Collections.shuffle(this.affectedBlockPositions, this.world.rand);
            for(BlockPos blockpos : this.affectedBlockPositions)
            {
                BlockState blockstate = this.world.getBlockState(blockpos);
                if (!blockstate.isAir(this.world, blockpos))
                {
                    BlockPos blockpos1 = blockpos.toImmutable();
                    this.world.getProfiler().startSection("explosion_blocks");
                    if (blockstate.canDropFromExplosion(this.world, blockpos, this) && this.world instanceof ServerWorld)
                    {
                        TileEntity tileentity = blockstate.hasTileEntity() ? this.world.getTileEntity(blockpos) : null;
                        LootContext.Builder lootcontext$builder = (new LootContext.Builder((ServerWorld)this.world)).withRandom(this.world.rand).withParameter(LootParameters.POSITION, blockpos).withParameter(LootParameters.TOOL, ItemStack.EMPTY).withNullableParameter(LootParameters.BLOCK_ENTITY, tileentity).withNullableParameter(LootParameters.THIS_ENTITY, this.exploder);
                        if (this.mode == Explosion.Mode.DESTROY)
                            lootcontext$builder.withParameter(LootParameters.EXPLOSION_RADIUS, this.size);
                        blockstate.getDrops(lootcontext$builder).forEach((p_229977_2_) -> handleExplosionDrops(objectarraylist, p_229977_2_, blockpos1));
                    }
                    blockstate.onBlockExploded(this.world, blockpos, this);
                    this.world.getProfiler().endSection();
                }
            }
            for(Pair<ItemStack, BlockPos> pair : objectarraylist)
                Block.spawnAsEntity(this.world, pair.getSecond(), pair.getFirst());
        }

        private static void handleExplosionDrops(ObjectArrayList<Pair<ItemStack, BlockPos>> dropPositionArray, ItemStack stack, BlockPos pos)
        {
            int i = dropPositionArray.size();
            for(int j = 0; j < i; ++j)
            {
                Pair<ItemStack, BlockPos> pair = dropPositionArray.get(j);
                ItemStack itemstack = pair.getFirst();
                if (ItemEntity.func_226532_a_(itemstack, stack))
                {
                    ItemStack itemstack1 = ItemEntity.func_226533_a_(itemstack, stack, 16);
                    dropPositionArray.set(j, Pair.of(itemstack1, pair.getSecond()));
                    if (stack.isEmpty())
                        return;
                }
            }
            dropPositionArray.add(Pair.of(stack, pos));
        }

        @Override
        public DamageSource getDamageSource()
        {
            return this.damageSource;
        }

        @Override
        public void setDamageSource(DamageSource damageSourceIn)
        {
            this.damageSource = damageSourceIn;
        }

        @Override
        public Map<PlayerEntity, Vec3d> getPlayerKnockbackMap()
        {
            return this.playerKnockbackMap;
        }

        @Override
        public void clearAffectedBlockPositions()
        {
            this.affectedBlockPositions.clear();
        }

        @Override
        public List<BlockPos> getAffectedBlockPositions()
        {
            return this.affectedBlockPositions;
        }

        @Override
        public Vec3d getPosition()
        {
            return this.position;
        }
    }

    public enum Classification
    {
        SNIPER(1),
        SNIPER_SEMI(1),
        RIFLE(1),
        SUB_MACHINEGUN(1),
        SHOTGUN(1),
        PISTOL(2),
        PISTOL_AUTO(2),
        MELEE_WEAPON(3),
        PROJECTILE(4),
        SPECIAL_WEAPON(5);

        private final int slotIndex;

        Classification(int slotIndex)
        {
            this.slotIndex = slotIndex;
        }

        public static Classification fromSlot(int index)
        {
            return Arrays.stream(values()).filter((classification) -> classification.getSlotIndex() == index).findFirst().orElse(SNIPER);
        }

        public String getDefaultFor()
        {
            switch (slotIndex)
            {
                case 1:
                    return "m4a1";
                case 2:
                    return "p226";
                case 3:
                    return "field_knife";
                case 4:
                    return "m18_grey";
                case 5:
                    return "binocular";
            }
            return "";
        }

        public int getSlotIndex()
        {
            return slotIndex - 1;
        }

        public void addToList(Item item)
        {
            switch (this)
            {
                case SNIPER:
                case SNIPER_SEMI:
                    Snipers.ITEM_SNIPERS.add(item);
                    break;
                case RIFLE:
                    Rifles.ITEM_RIFLES.add(item);
                    break;
                case SUB_MACHINEGUN:
                case SHOTGUN:
                    SubmachineGuns.ITEM_SUBMACHINE_GUNS.add(item);
                    break;
                case PISTOL:
                case PISTOL_AUTO:
                    Pistols.ITEM_PISTOLS.add(item);
                    break;
                case MELEE_WEAPON:
                    MeleeWeapons.ITEM_MELEE_WEAPONS.add(item);
                case PROJECTILE:
                    Projectiles.ITEM_PROJECTILES.add(item);
                    break;
                case SPECIAL_WEAPON:
                    SpecialWeapons.ITEM_SPECIAL_WEAPONS.add(item);
                    break;
            }
        }

        public boolean isTypeEnabled()
        {
            switch (this)
            {
                case SNIPER:
                    return ENABLE_SNIPERS.get();
                case SNIPER_SEMI:
                    return ENABLE_SEMI_SNIPERS.get();
                case RIFLE:
                    return ENABLE_RIFLES.get();
                case SUB_MACHINEGUN:
                    return ENABLE_SUB_MACHINEGUNS.get();
                case SHOTGUN:
                    return ENABLE_SHOTGUNS.get();
                case PISTOL:
                    return ENABLE_PISTOLS.get();
                case PISTOL_AUTO:
                    return ENABLE_AUTO_PISTOLS.get();
                case MELEE_WEAPON:
                    return true;
                case PROJECTILE:
                    return ENABLE_PROJECTILES.get();
                case SPECIAL_WEAPON:
                    return ENABLE_SPECIAL_WEAPONS.get();
            }
            return false;
        }

        public boolean validate(PlayerEntity player)
        {
            if (!player.abilities.isCreativeMode && !player.world.isRemote() && !isTypeEnabled())
            {
                player.sendMessage(new StringTextComponent("Current type of weapon is disabled by the server!"));
                return false;
            }
            return true;
        }
    }
}
