package pellucid.ava.misc.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.CommandSource;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;

import java.util.Collection;

import static net.minecraft.command.Commands.argument;
import static net.minecraft.command.Commands.literal;

public class BoostPlayerCommand
{
    public static ArgumentBuilder<CommandSource, LiteralArgumentBuilder<CommandSource>> register(CommandDispatcher<CommandSource> dispatcher)
    {
        return literal("playerBoosts")
                .requires(player -> player.hasPermissionLevel(2))
                .then(literal("attackDamage")
                .then(literal("add")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(-20, 20))
                .executes((context) -> add(true, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets"))))))
                .then(literal("set")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(0, 20))
                .executes((context) -> set(true, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets")))))))
                .then(literal("health")
                .then(literal("add")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(-20, 20))
                .executes((context) -> add(false, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets"))))))
                .then(literal("set")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(0, 20))
                .executes((context) -> set(false, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets")))))));
    }

    private static int add(boolean attackDamage, int amount, Collection<ServerPlayerEntity> players)
    {
        players.forEach((player) -> {
            IPlayerAction cap = PlayerAction.getCap(player);
            if (attackDamage)
                cap.setAttackDamageBoost(cap.getAttackDamageBoost() + amount);
            else
                cap.setHealthBoost(cap.getHealthBoost() + amount);
            player.sendMessage(new StringTextComponent("Your " + (attackDamage ? "attack damage" : "health") + " boost has been changed by " + amount + ", currently on level " + (attackDamage ? cap.getAttackDamageBoost() : cap.getHealthBoost())));
        });
        return amount;
    }

    private static int set(boolean attackDamage, int amount, Collection<ServerPlayerEntity> players)
    {
        players.forEach((player) -> {
            IPlayerAction cap = PlayerAction.getCap(player);
            if (attackDamage)
                cap.setAttackDamageBoost(amount);
            else
                cap.setHealthBoost(amount);
            player.sendMessage(new StringTextComponent("Your " + (attackDamage ? "attack damage" : "health") + " boost has been set to " + amount + ", currently on level " + (attackDamage ? cap.getAttackDamageBoost() : cap.getHealthBoost())));
        });
        return amount;
    }
}
