package pellucid.ava.misc.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.world.server.ServerWorld;
import pellucid.ava.misc.cap.WorldData;

public class SetMobDropsKitsCommand
{
    public static ArgumentBuilder<CommandSource, LiteralArgumentBuilder<CommandSource>> register(CommandDispatcher<CommandSource> dispatcher)
    {
        return Commands.literal("setMobDropKitChance")
                .requires(player -> player.hasPermissionLevel(3))
                .then(Commands.argument("value", FloatArgumentType.floatArg(0, 1))
                .executes((context) ->
                {
                    float chance = FloatArgumentType.getFloat(context, "value");
                    for (ServerWorld world : context.getSource().getServer().getWorlds())
                        WorldData.getCap(world).setMobDropsKitsChance(chance);
                    return 1;
                }));
    }
}
